<?php

use Illuminate\Database\Seeder;
use App\Models\ConfigId;

class ConfigIdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
      $this->command->info("Hapus Config IDS");
    	DB::table('config_ids')->delete();

      $this->command->info("Simpan Config IDS");
      $config = ConfigId::firstOrNew(array(
        'table_source'=>'provinsi',
        'config_name'=>'JAWA_TIMUR',
      )
      );
      $config->config_value='15';
      $config->description='Provinsi Jawa Timur';
      $config->save();

      $config = ConfigId::firstOrNew(array(
        'table_source'=>'kabupaten',
        'config_name'=>'SAMPANG',
      )
      );
      $config->config_value='1';
      $config->description='Kabupaten Sampang';
      $config->save();

    }
}
