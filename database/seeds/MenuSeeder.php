<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Menu;
use App\Permission;

class MenuSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $this->command->info('Delete semua tabel menu');
    Model::unguard();
    Menu::truncate();
    $this->menuAcl();
    $this->menuMaster();
    $this->menuBarang();
    $this->menuWarung();
    $this->menuInformasi();
    $this->menuVerifikasi();
  }

  private function menuAcl()
  {
    $this->command->info('Menu ACL Seeder');
    $permission = Permission::firstOrNew(array(
      'name' => 'read-acl-menu'
    ));
    $permission->display_name = 'Read ACL Menus';
    $permission->save();
    $menu = Menu::firstOrNew(array(
      'name' => 'ACL',
      'permission_id' => $permission->id,
      'ordinal' => 1,
      'parent_status' => 'Y'
    ));
    $menu->icon = 'md-settings';
    $menu->save();

    //create SUBMENU master
    $permission = Permission::firstOrNew(array(
      'name' => 'read-users',
    ));
    $permission->display_name = 'Read Users';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Users',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'user',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-permissions',
    ));
    $permission->display_name = 'Read Permissions';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Permissions',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'permission',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-menus',
    ));
    $permission->display_name = 'Read Menus';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Menus',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'menu',
      )
    );
    $submenu->save();



    $permission = Permission::firstOrNew(array(
      'name' => 'read-roles',
    ));
    $permission->display_name = 'Read Roles';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Roles',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'role',
      )
    );
    $submenu->save();
  }
  private function menuMaster()
  {
    $this->command->info('Menu Master Seeder');
    $permission = Permission::firstOrNew(array(
      'name' => 'read-master-menu'
    ));
    $permission->display_name = 'Read Master Menus';
    $permission->save();
    $menu = Menu::firstOrNew(array(
      'name' => 'Master Data',
      'permission_id' => $permission->id,
      'ordinal' => 1,
      'parent_status' => 'Y'
    ));
    $menu->icon = 'md-folder';
    $menu->save();


    //create SUBMENU master
    $permission = Permission::firstOrNew(array(
      'name' => 'read-provinsi',
    ));
    $permission->display_name = 'Read Provinsi';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Provinsi',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'provinsi',
      )
    );
    $submenu->save();
    $permission = Permission::firstOrNew(array(
      'name' => 'read-kabupaten',
    ));
    $permission->display_name = 'Read Kabupaten';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Kabupaten',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'kabupaten',
      )
    );
    $submenu->save();
    $permission = Permission::firstOrNew(array(
      'name' => 'read-kecamatan',
    ));
    $permission->display_name = 'Read Kecamatan';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Kecamatan',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'kecamatan',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-kelurahan',
    ));
    $permission->display_name = 'Read Kelurahan';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Kelurahan',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'kelurahan',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-grosir',
    ));
    $permission->display_name = 'Read Grosir';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Grosir',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'grosir',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-minimal-order',
    ));
    $permission->display_name = 'Read minimal order';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Minimal Order',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'minimal-order',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-bumdes',
    ));
    $permission->display_name = 'Read bumdes';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Bumdes',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'bumde',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-presentase-komisi',
    ));
    $permission->display_name = 'Read presentase-komisi';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Presentase Komisi',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'presentase-komisi',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-paket-promo',
    ));
    $permission->display_name = 'Read paket-promo';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Paket Promo',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'paket-promo',
      )
    );
    $submenu->save();
  }
  private function menuBarang()
  {
    $this->command->info('Menu Barang Seeder');
    $permission = Permission::firstOrNew(array(
      'name' => 'read-barang-menu'
    ));
    $permission->display_name = 'Read Barang Menus';
    $permission->save();
    $menu = Menu::firstOrNew(array(
      'name' => 'Master Barang',
      'permission_id' => $permission->id,
      'ordinal' => 1,
      'parent_status' => 'Y'
    ));
    $menu->icon = 'md-archive';
    $menu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-kategori-barang',
    ));
    $permission->display_name = 'Read Kategori Barang';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Kategori Barang',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'kategori-barang',
      )
    );
    $submenu->save();
    $permission = Permission::firstOrNew(array(
      'name' => 'read-sub-kategori-barang',
    ));
    $permission->display_name = 'Read Sub Kategori Barang';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Sub Kategori Barang',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'sub-kategori-barang',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-barang',
    ));
    $permission->display_name = 'Read Barang';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Barang',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'barang',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-skema-grosir',
    ));
    $permission->display_name = 'Read Skema Grosir';
    $permission->save();
    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Skema Grosir',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'skema-grosir',
      )
    );
    $submenu->save();
  }
  private function menuWarung()
  {
    $this->command->info('Menu Warung Seeder');
    $permission = Permission::firstOrNew(array(
      'name' => 'read-warung-menu'
    ));
    $permission->display_name = 'Read Warung Menus';
    $permission->save();
    $menu = Menu::firstOrNew(array(
      'name' => 'Master Warung',
      'permission_id' => $permission->id,
      'ordinal' => 1,
      'parent_status' => 'N',
      'url' => 'warung',
    ));
    $menu->icon = 'md-store';
    $menu->save();
  }

  private function menuInformasi()
  {
    $this->command->info('Menu Informasi Seeder');
    $permission = Permission::firstOrNew(array(
      'name' => 'read-informasi-menu'
    ));
    $permission->display_name = 'Read Informasi Menus';
    $permission->save();
    $menu = Menu::firstOrNew(array(
      'name' => 'Informasi',
      'permission_id' => $permission->id,
      'ordinal' => 1,
      'parent_status' => 'Y'
    ));
    $menu->icon = 'md-info';
    $menu->save();


    //create SUBMENU 
    $permission = Permission::firstOrNew(array(
      'name' => 'read-user-apk',
    ));
    $permission->display_name = 'Read User Apk';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Pengguna Android',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'user-apk',
      )
    );
    $submenu->save();

    $permission = Permission::firstOrNew(array(
      'name' => 'read-informasi-pembayaran',
    ));
    $permission->display_name = 'Read informasi pembayaran';
    $permission->save();

    $submenu = Menu::firstOrNew(
      array(
        'name' => 'Pembayaran',
        'parent_id' => $menu->id,
        'permission_id' => $permission->id,
        'ordinal' => 2,
        'parent_status' => 'N',
        'url' => 'pembayaran/info',
      )
    );
    $submenu->save();
  }
  private function menuVerifikasi()
  {
    $this->command->info('Menu Verifikasi Seeder');
    $permission = Permission::firstOrNew(array(
      'name' => 'read-verifikasi-menu'
    ));
    $permission->display_name = 'Read Verifikasi Menus';
    $permission->save();
    $menu = Menu::firstOrNew(array(
      'name' => 'Verifikasi Order',
      'permission_id' => $permission->id,
      'ordinal' => 1,
      'parent_status' => 'N',
      'url' => 'order-datum',
    ));
    $menu->icon = 'md-check';
    $menu->save();

    $this->command->info('Menu Pembayaran Seeder');
    $permission = Permission::firstOrNew(array(
      'name' => 'read-verifikasi-menu'
    ));
    $permission->display_name = 'Read Pembayaran Menus';
    $permission->save();
    $menu = Menu::firstOrNew(array(
      'name' => 'Pembayaran',
      'permission_id' => $permission->id,
      'ordinal' => 1,
      'parent_status' => 'N',
      'url' => 'pembayaran',
    ));
    $menu->icon = 'md-money-box';
    $menu->save();
  }
}
