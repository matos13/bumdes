<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBumdesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bumdes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bumdes',225);
            $table->string('nama_koordinator',225);
            $table->text('alamat')->nullable();;
            $table->integer('id_kelurahan')->unsigned()->nullable();
            $table->string('status_aktif',1)->default('Y');   
            $table->timestamps();
            $table->foreign('id_kelurahan')->references('id')->on('kelurahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bumdes');
    }
}
