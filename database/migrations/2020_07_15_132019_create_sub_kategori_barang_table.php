<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubKategoriBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_kategori_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategori_barang')->unsigned()->nullable();
            $table->string('sub_kategori_barang',255)->nullable();
            $table->text('icon')->nullable();
            $table->string('flag_aktif',1)->default('Y');
            $table->timestamps();
            $table->foreign('id_kategori_barang')->references('id')->on('kategori_barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_kategori_barang');
    }
}
