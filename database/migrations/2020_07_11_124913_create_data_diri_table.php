<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataDiriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_diri', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user_apk')->unsigned()->nullable();
            $table->string('nik',100)->nullable();
            $table->string('jenis_kelamin',100)->nullable();
            $table->string('tempat_lahir',100)->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->text('alamat_pemilik')->nullable();
            $table->string('nama_pemilik',100)->nullable();
            $table->text('foto_ktp')->nullable();
            $table->integer('id_warung')->unsigned()->nullable();
            $table->foreign('id_user_apk')->references('id')->on('user_apk');
            $table->foreign('id_warung')->references('id')->on('warung');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_diri');
    }
}
