<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdOrderToKomisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('komisi', function (Blueprint $table) {
            $table->integer('id_order')->unsigned()->nullable();
            $table->foreign('id_order')->references('id')->on('order_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komisi', function (Blueprint $table) {
            //
        });
    }
}
