<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkemaGrosirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skema_grosir', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_barang')->unsigned()->nullable();
            $table->integer('jumlah_minimal')->nullable();
            $table->integer('jumlah_maksimal')->nullable();
            $table->double('harga_perbarang', 10, 2);
            $table->string('flag_aktif',1)->default('Y');
            $table->timestamps();
            $table->foreign('id_barang')->references('id')->on('barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skema_grosir');
    }
}
