<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warung', function (Blueprint $table) {
            $table->increments('id');
            $table->string('warung',255)->nullable();
            $table->text('alamat')->nullable();
            $table->integer('id_grosir')->unsigned()->nullable();
            $table->integer('id_kelurahan')->unsigned()->nullable();
            $table->string('flag_aktif',1)->default('Y');
            $table->timestamps();
            $table->foreign('id_grosir')->references('id')->on('grosir');
            $table->foreign('id_kelurahan')->references('id')->on('kelurahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warung');
    }
}
