<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPemilikToWarungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warung', function (Blueprint $table) {
            $table->string('nama_ktp', 255)->nullable();
            $table->string('nik', 20)->nullable();
            $table->string('jenis_kelamin', 1)->nullable();
            $table->string('tempat_lahir', 100)->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->text('alamat_ktp')->nullable();
            $table->string('nama_pemilik', 255)->nullable();
            $table->text('alamat_pemilik')->nullable();
            $table->string('ktp', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warung', function (Blueprint $table) {
            //
        });
    }
}
