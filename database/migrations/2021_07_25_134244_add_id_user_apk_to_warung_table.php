<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdUserApkToWarungTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('warung', function (Blueprint $table) {
            //
            $table->integer('id_user_apk')->unsigned()->nullable();
            $table->string('no_hp', 20)->nullable();
            $table->foreign('id_user_apk')->references('id')->on('user_apk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warung', function (Blueprint $table) {
            //
        });
    }
}
