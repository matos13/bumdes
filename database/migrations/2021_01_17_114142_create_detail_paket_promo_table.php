<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPaketPromoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_paket_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('tanggal_awal')->nullable();
            $table->timestamp('tanggal_akhir')->nullable();
            $table->string('status_aktif',1)->default('Y');
            $table->integer('id_paket_promo')->unsigned()->nullable();   
            $table->integer('id_barang')->unsigned()->nullable();   
            $table->timestamps();
            $table->foreign('id_paket_promo')->references('id')->on('paket_promo');
            $table->foreign('id_barang')->references('id')->on('barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_paket_promo');
    }
}
