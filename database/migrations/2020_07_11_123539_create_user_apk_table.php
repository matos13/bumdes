<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserApkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_apk', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',100)->nullable();
            $table->string('nohp',15)->nullable();
            $table->integer('id_kelurahan')->unsigned()->nullable();
            $table->string('status',10)->default('Grosir');
            $table->string('password');
            $table->timestamps();
            $table->foreign('id_kelurahan')->references('id')->on('kelurahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_apk');
    }
}
