<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barang',255)->nullable();
            $table->integer('id_grosir')->unsigned()->nullable();
            $table->integer('id_kategori_barang')->unsigned()->nullable();
            $table->double('harga_normal', 10, 2);
            $table->string('flag_aktif',1)->default('Y');
            $table->timestamps();
            $table->foreign('id_grosir')->references('id')->on('grosir');
            $table->foreign('id_kategori_barang')->references('id')->on('kategori_barang');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang');
    }
}
