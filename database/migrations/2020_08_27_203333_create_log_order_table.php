<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_order')->unsigned()->nullable();
            $table->string('status',255)->nullable();
            $table->text('keterangan')->nullable();
            $table->timestamps();
            $table->foreign('id_order')->references('id')->on('order_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_order');
    }
}
