<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user_apk')->unsigned()->nullable();
            $table->string('status_order',100)->nullable();
            $table->timestamp('tanggal_order')->nullable();
            $table->string('latitude',100)->nullable();
            $table->string('longitude',100)->nullable();
            $table->integer('total')->nullable();
            $table->timestamps();
            $table->foreign('id_user_apk')->references('id')->on('user_apk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_data');
    }
}
