<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komisi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_bumdes')->unsigned()->nullable();
            $table->double('total_belanja', 10, 2);
            $table->double('presentase_komisi', 10, 2);   
            $table->double('total_komisi', 10, 2);
            $table->timestamp('tanggal_komisi')->nullable();
            $table->string('status_bayar',1)->default('N');
            $table->timestamps();
            $table->foreign('id_bumdes')->references('id')->on('bumdes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komisi');
    }
}
