<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketPromoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paket_promo', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paket',225);
            $table->text('gambar');
            $table->timestamp('tanggal_awal')->nullable();
            $table->timestamp('tanggal_akhir')->nullable();
            $table->string('status_aktif',1)->default('Y');
            $table->double('total_harga', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paket_promo');
    }
}
