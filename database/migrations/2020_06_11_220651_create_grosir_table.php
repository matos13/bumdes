<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrosirTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grosir', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grosir',255)->nullable();
            $table->integer('id_kelurahan')->unsigned()->nullable();
            $table->string('alamat',400)->nullable();
            $table->string('flag_aktif',1)->default('Y');
            $table->timestamps();
            $table->foreign('id_kelurahan')->references('id')->on('kelurahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grosir');
    }
}
