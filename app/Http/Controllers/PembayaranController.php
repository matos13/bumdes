<?php

namespace App\Http\Controllers;

use App\Models\OrderDatum;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

date_default_timezone_set("Asia/Jakarta");

class PembayaranController extends Controller
{
    public $viewDir = "barang";
    public $breadcrumbs = array(
        'permissions' => array('title' => 'Pembayaran', 'link' => "#", 'active' => false, 'display' => true),
    );

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        return view("pembayaran/index");
    }
    public function info()
    {
        return view("pembayaran/info");
    }
    public function detail($kode)
    {
        $dataList = DB::select("SELECT od.*, g.grosir,b.barang,do.qty,do.harga,do.subtotal from order_data od join grosir g on od.id_grosir = g.id 
        join detail_order do on do.id_order = od.id join barang b on b.id = do.id_barang
        where od.id = $kode");
        // dd($dataList);
        return view("pembayaran/detail", ['data' => $dataList]);
    }
    public function cetak($kode)
    {
        $data['data'] = DB::select("SELECT od.*, g.grosir,b.barang,do.qty,do.harga,do.subtotal from order_data od join grosir g on od.id_grosir = g.id 
        join detail_order do on do.id_order = od.id join barang b on b.id = do.id_barang
        where od.id = $kode");
        // dd($dataList);
        // $details =['title' => 'Invoice'];
        $pdf = PDF::loadview('cetak/invoice', $data)->setPaper('A4', 'potrait');
        return $pdf->stream();
    }
    public function simpan($kode)
    {
        $data = OrderDatum::find($kode);
        $status = $data->update(['status_bayar_bumdes' => 'Y']);
        message($status, 'Pembayaran berhasil', 'Pembayaran Gagal');
        return redirect('/pembayaran');
    }
    public function loadData()
    {
        $GLOBALS['nomor'] = \Request::input('start', 1) + 1;
        $dataList = DB::select("SELECT od.*, g.grosir from order_data od join grosir g on od.id_grosir = g.id where status_order ='pengiriman_pesanan' and status_bayar_bumdes='N' and status_bayar ='status_bayar_warung' ");
        // dd($dataList);
        if (request()->get('status') == 'trash') {
            $dataList->onlyTrashed();
        }
        return Datatables::of($dataList)
            ->addColumn('nomor', function ($kategori) {
                return $GLOBALS['nomor']++;
            })
            ->addColumn('total', function ($data) {
                return rupiahTanpaKoma($data->total);
            })
            ->addColumn('action', function ($data) {
                $content = '<button class="btn btn-xs btn-info" onclick="detail(' . $data->id . ')"><i class="icon fa-bars"></i>Detail</button>';
                return $content;
            })
            ->make(true);
    }
    public function loadData2()
    {
        $GLOBALS['nomor'] = \Request::input('start', 1) + 1;
        $dataList = DB::select("SELECT od.*, g.grosir from order_data od join grosir g on od.id_grosir = g.id where status_order ='dipesan' and status_bayar_bumdes='Y'");
        // dd($dataList);
        if (request()->get('status') == 'trash') {
            $dataList->onlyTrashed();
        }
        return Datatables::of($dataList)
            ->addColumn('nomor', function ($kategori) {
                return $GLOBALS['nomor']++;
            })
            ->addColumn('total', function ($data) {
                return rupiahTanpaKoma($data->total);
            })
            ->addColumn('action', function ($data) {
                $content = '<button class="btn btn-xs btn-info" onclick="cetak(' . $data->id . ')"><i class="icon fa-print"></i>cetak invoice</button>';
                return $content;
            })
            ->make(true);
    }
}
