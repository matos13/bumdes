<?php
namespace App\Http\Controllers;

use App\Models\OrderDatum;
use App\Models\LogOrder;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use DB;

class OrderDatumController extends Controller
{
    public $viewDir = "order_datum";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Order-data','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-order-data');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['orderDatum' => new OrderDatum]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, OrderDatum::validationRules());

           $act=OrderDatum::create($request->all());
           message($act,'Data Order Data berhasil ditambahkan','Data Order Data gagal ditambahkan');
           return redirect('order-datum');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $orderDatum=OrderDatum::find($kode);
           return $this->view("show",['orderDatum' => $orderDatum]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $orderDatum=OrderDatum::find($kode);
           return $this->view( "form", ['orderDatum' => $orderDatum] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $orderDatum=OrderDatum::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, OrderDatum::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $orderDatum->update($data);
               return "Record updated";
           }
           $this->validate($request, OrderDatum::validationRules());

           $act=$orderDatum->update($request->all());
           message($act,'Data Order Data berhasil diupdate','Data Order Data gagal diupdate');

           return redirect('/order-datum');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $orderDatum=OrderDatum::find($kode);
           $act=false;
           try {
               $act=$orderDatum->forceDelete();
           } catch (\Exception $e) {
               $orderDatum=OrderDatum::find($orderDatum->pk());
               $act=$orderDatum->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
        //    $dataList = OrderDatum::select('*');
        $dataList = OrderDatum::select('order_data.*','data_diri.nama_pemilik','warung.warung')
                    ->join('user_apk','user_apk.id','=','order_data.id_user_apk')
                    ->join('data_diri','user_apk.id','=','data_diri.id_user_apk')
                    ->join('warung','warung.id','=','data_diri.id_warung');
                    
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('aksi', function ($data) {
                    return array('id'=>$data->pk(),'status_order'=>$data->status_order);        
                 })
            //    ->addColumn('action', function ($data) {
            //        $edit=url("order-datum/".$data->pk())."/edit";
            //        $detail=url("order-datum/detail/".$data->pk());
            //        $delete=url("order-datum/".$data->pk());
            //      $content = '';
            //     //   $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
            //     //   $content .= " <a onclick='detail(\"$detail\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Detail'><i class='icon md-menu' style='color:blue;'></i></a>";

            //         $content="<button onclick='detail(\"$detail\")' class='btn  btn-info btn-sm waves-effect waves-classic' > <i class='icon md-menu'></i>Detail</button>";
            //        return $content;
            //    })
               ->make(true);
       }
       
       public function detail(Request $request, $kode){
        $data=DB::table('order_data')
        ->join('user_apk','user_apk.id','=','order_data.id_user_apk')
        ->join('data_diri','user_apk.id','=','data_diri.id_user_apk')
        ->join('warung','warung.id','=','data_diri.id_warung')
        ->join('detail_order','order_data.id','=','detail_order.id_order')
        ->join('barang','barang.id','=','detail_order.id_barang')
        ->where('order_data.id','=',$kode)
        ->select('data_diri.nama_pemilik','warung.warung','order_data.id','order_data.nomor_order','order_data.tanggal_order','order_data.status_order','detail_order.qty','detail_order.harga','detail_order.subtotal','barang.barang')->get();
   
        return $this->view( "detail", ['data' => $data] );
        }
        public function confirm(Request $request, $kode){
            DB::beginTransaction();
            try {
                $log = array('id_order' =>$kode ,'status'=>'disiapkan','keterangan'=>'pesanan disiapkan' );
                $act=LogOrder::create($log);
                $kategoriBarang=OrderDatum::find($kode);
                $data=array('status_order'=>'disiapkan');
                $status=$kategoriBarang->update($data);
                DB::commit();
                message($status,'Verifikasi Order Berhasil','Verifikasi Order Gagal ');    
                return redirect('/order-datum');
            } catch (\Throwable $th) {
                return redirect('/order-datum');
                DB::rollBack();
            }
        }
        public function cetak(Request $request, $kode){
            $data=DB::table('order_data')
            ->join('user_apk','user_apk.id','=','order_data.id_user_apk')
            ->join('data_diri','user_apk.id','=','data_diri.id_user_apk')
            ->join('warung','warung.id','=','data_diri.id_warung')
            ->join('detail_order','order_data.id','=','detail_order.id_order')
            ->join('barang','barang.id','=','detail_order.id_barang')
            ->where('order_data.id','=',$kode)
            ->select('data_diri.nama_pemilik','warung.warung','order_data.id','order_data.nomor_order','order_data.tanggal_order','order_data.status_order','detail_order.qty','detail_order.harga','detail_order.subtotal','barang.barang')->get();
    
            return $this->view( "cetak", ['data' => $data] );
        }
}
