<?php
namespace App\Http\Controllers;

use App\Models\DataDiri;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class DataDiriController extends Controller
{
    public $viewDir = "data_diri";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Data-diri','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-data-diri');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['dataDiri' => new DataDiri]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, DataDiri::validationRules());

           $act=DataDiri::create($request->all());
           message($act,'Data Data Diri berhasil ditambahkan','Data Data Diri gagal ditambahkan');
           return redirect('data-diri');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $dataDiri=DataDiri::find($kode);
           return $this->view("show",['dataDiri' => $dataDiri]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $dataDiri=DataDiri::find($kode);
           return $this->view( "form", ['dataDiri' => $dataDiri] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $dataDiri=DataDiri::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, DataDiri::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $dataDiri->update($data);
               return "Record updated";
           }
           $this->validate($request, DataDiri::validationRules());

           $act=$dataDiri->update($request->all());
           message($act,'Data Data Diri berhasil diupdate','Data Data Diri gagal diupdate');

           return redirect('/data-diri');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $dataDiri=DataDiri::find($kode);
           $act=false;
           try {
               $act=$dataDiri->forceDelete();
           } catch (\Exception $e) {
               $dataDiri=DataDiri::find($dataDiri->pk());
               $act=$dataDiri->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = DataDiri::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("data-diri/".$data->pk())."/edit";
                   $delete=url("data-diri/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
