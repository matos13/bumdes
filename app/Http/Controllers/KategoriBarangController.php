<?php
namespace App\Http\Controllers;

use App\Models\KategoriBarang;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class KategoriBarangController extends Controller
{
    public $viewDir = "kategori_barang";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Kategori-barang','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-kategori-barang');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['kategoriBarang' => new KategoriBarang]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, KategoriBarang::validationRules());
           $insert['kategori_barang']=$request->all()['kategori_barang'];
           $insert['flag_aktif']=$request->all()['flag_aktif'];
           if ($files = $request->file('icon')) {
            $destinationPath = 'public/images/kategori-barang-icon'; // upload path
            $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move($destinationPath, $profileImage);
            $insert['icon'] = "$profileImage";
            }
        //  dd($request->all());

           $act=KategoriBarang::create($insert);
           message($act,'Data Kategori Barang berhasil ditambahkan','Data Kategori Barang gagal ditambahkan');
           return redirect('kategori-barang');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $kategoriBarang=KategoriBarang::find($kode);
           return $this->view("show",['kategoriBarang' => $kategoriBarang]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $kategoriBarang=KategoriBarang::find($kode);
           return $this->view( "form", ['kategoriBarang' => $kategoriBarang] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $kategoriBarang=KategoriBarang::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, KategoriBarang::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $kategoriBarang->update($data);
               return "Record updated";
           }
           $this->validate($request, KategoriBarang::validationRules());
           $insert['kategori_barang']=$request->all()['kategori_barang'];
           $insert['flag_aktif']=$request->all()['flag_aktif'];
           if ($files = $request->file('icon')) {
                $destinationPath = 'public/images/kategori-barang-icon'; // upload path
                $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $insert['icon'] = "$profileImage";
            }
           $act=$kategoriBarang->update($insert);
           message($act,'Data Kategori Barang berhasil diupdate','Data Kategori Barang gagal diupdate');

           return redirect('/kategori-barang');
       }
       public function activate(Request $request, $kode)
       {
        // dd($kode);
            $kategoriBarang= KategoriBarang::find($kode);
            $data=array(
            'flag_aktif'=>'Y',
            );
            
            $status=$kategoriBarang->update($data);
            message($status,'Kategori Barang Berhasil Diaktifkan Kembali','Kategori Barang Gagal Diaktifkan Kembali');
            return redirect('/kategori-barang');
       } 

        public function deactivate(Request $request, $kode){
        
            $kategoriBarang=KategoriBarang::find($kode);
            $data=array('flag_aktif'=>'N',);
       
            $status=$kategoriBarang->update($data);
            message($status,'Kategori Barang Berhasil Dinonaktifkan','Kategori Barang Gagal Dinonaktifkan');
        
            return redirect('/kategori-barang');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $kategoriBarang=KategoriBarang::find($kode);
           $act=false;
           try {
               $act=$kategoriBarang->forceDelete();
           } catch (\Exception $e) {
               $kategoriBarang=KategoriBarang::find($kategoriBarang->pk());
               $act=$kategoriBarang->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = KategoriBarang::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('status', function ($data) {
                if(isset($data->flag_aktif))
                {
                 return array('id'=>$data->pk(),'flag_aktif'=>$data->flag_aktif);
                }else
                {
                 return null;
                }
                
            })
               ->addColumn('action', function ($data) {
                   $edit=url("kategori-barang/".$data->pk())."/edit";
                   $delete=url("kategori-barang/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
