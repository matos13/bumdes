<?php
namespace App\Http\Controllers;

use App\Models\Grosir;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class GrosirController extends Controller
{
    public $viewDir = "grosir";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Grosir','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-grosir');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['grosir' => new Grosir]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, Grosir::validationRules());

           $act=Grosir::create($request->all());
           message($act,'Data Grosir berhasil ditambahkan','Data Grosir gagal ditambahkan');
           return redirect('grosir');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $grosir=Grosir::find($kode);
           return $this->view("show",['grosir' => $grosir]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $grosir=Grosir::find($kode);
           return $this->view( "form", ['grosir' => $grosir] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $grosir=Grosir::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, Grosir::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $grosir->update($data);
               return "Record updated";
           }
           $this->validate($request, Grosir::validationRules());

           $act=$grosir->update($request->all());
           message($act,'Data Grosir berhasil diupdate','Data Grosir gagal diupdate');

           return redirect('/grosir');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $grosir=Grosir::find($kode);
           $act=false;
           try {
               $act=$grosir->forceDelete();
           } catch (\Exception $e) {
               $grosir=Grosir::find($grosir->pk());
               $act=$grosir->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = Grosir::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('status',function($data){
                if ($data->flag_aktif=='Y') {
                    $status='Aktif';
                 }else{
                     $status='Tidak Aktif';
                }
                return $status;
                 })
                 ->addColumn('kelurahan',function($data){
                    return $data->kelurahan->kelurahan;
                })
               ->addColumn('action', function ($data) {
                   $edit=url("grosir/".$data->pk())."/edit";
                   $delete=url("grosir/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
