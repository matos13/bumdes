<?php

namespace App\Http\Controllers;

use App\Models\SkemaGrosir;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Facades\Auth;


class SkemaGrosirController extends Controller
{
    public $viewDir = "skema_grosir";
    public $breadcrumbs = array(
        'permissions' => array('title' => 'Skema-grosir', 'link' => "#", 'active' => false, 'display' => true),
    );

    public function __construct()
    {
        $this->middleware('permission:read-skema-grosir');
    }

    public function index()
    {
        return $this->view("index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->view("form", ['skemaGrosir' => new SkemaGrosir]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //    dd($_POST);
        $this->validate($request, SkemaGrosir::validationRules());
        $data = array(
            'id_barang' => $request->all()['id_barang'],
            'jumlah_minimal' => $request->all()['jumlah_minimal'],
            'jumlah_maksimal' => $request->all()['jumlah_maksimal'],
            'harga_perbarang' => str_replace(".", "", $request->all()['harga_perbarang']),
            'flag_aktif' => $request->all()['flag_aktif'],
        );
        $act = SkemaGrosir::create($data);
        message($act, 'Data Skema Grosir berhasil ditambahkan', 'Data Skema Grosir gagal ditambahkan');
        return redirect('skema-grosir');
    }

    /**
     * Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, $kode)
    {
        $skemaGrosir = SkemaGrosir::find($kode);
        return $this->view("show", ['skemaGrosir' => $skemaGrosir]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, $kode)
    {
        $skemaGrosir = SkemaGrosir::find($kode);
        //    $id_grosir=Auth::user()->id_grosir;
        return $this->view("form", ['skemaGrosir' => $skemaGrosir]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        $skemaGrosir = SkemaGrosir::find($kode);
        if ($request->isXmlHttpRequest()) {
            $data = [$request->name  => $request->value];
            $validator = \Validator::make($data, SkemaGrosir::validationRules($request->name));
            if ($validator->fails())
                return response($validator->errors()->first($request->name), 403);
            $skemaGrosir->update($data);
            return "Record updated";
        }
        $this->validate($request, SkemaGrosir::validationRules());
        $data = array(
            'id_barang' => $request->all()['id_barang'],
            'jumlah_minimal' => $request->all()['jumlah_minimal'],
            'jumlah_maksimal' => $request->all()['jumlah_maksimal'],
            'harga_perbarang' => str_replace(".", "", $request->all()['harga_perbarang']),
            'flag_aktif' => $request->all()['flag_aktif'],
        );
        $act = $skemaGrosir->update($data);
        message($act, 'Data Skema Grosir berhasil diupdate', 'Data Skema Grosir gagal diupdate');

        return redirect('/skema-grosir');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Request $request, $kode)
    {
        $skemaGrosir = SkemaGrosir::find($kode);
        $act = false;
        try {
            $act = $skemaGrosir->forceDelete();
        } catch (\Exception $e) {
            $skemaGrosir = SkemaGrosir::find($skemaGrosir->pk());
            $act = $skemaGrosir->delete();
        }
    }

    protected function view($view, $data = [])
    {
        return view($this->viewDir . "." . $view, $data);
    }
    public function loadData()
    {
        $GLOBALS['nomor'] = \Request::input('start', 1) + 1;
        $id_grosir = Auth::user()->id_grosir;
        if ($id_grosir) {
            $dataList = SkemaGrosir::select('skema_grosir.*', 'barang.barang')->join('barang', 'barang.id', '=', 'skema_grosir.id_barang')->where('barang.id_grosir', $id_grosir)->orderBy('barang', 'ASC')->orderBy('skema_grosir.jumlah_minimal', 'ASC');
        } else {
            $dataList = SkemaGrosir::select('skema_grosir.*', 'barang.barang')->join('barang', 'barang.id', '=', 'skema_grosir.id_barang')->orderBy('barang', 'ASC')->orderBy('skema_grosir.jumlah_minimal', 'ASC');
        }
        if (request()->get('status') == 'trash') {
            $dataList->onlyTrashed();
        }
        return Datatables::of($dataList)
            ->addColumn('nomor', function ($kategori) {
                return $GLOBALS['nomor']++;
            })
            ->addColumn('harga_perbarang', function ($data) {
                return rupiahTanpaKoma($data->harga_perbarang);
            })
            ->addColumn('status', function ($data) {
                if (isset($data->flag_aktif)) {
                    return array('id' => $data->pk(), 'flag_aktif' => $data->flag_aktif);
                } else {
                    return null;
                }
            })
            ->addColumn('barang', function ($data) {
                return $data->barang;
            })
            ->addColumn('action', function ($data) {
                $edit = url("skema-grosir/" . $data->id) . "/edit";
                $delete = url("skema-grosir/" . $data->id);
                $content = '';
                $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                return $content;
            })
            ->make(true);
    }
    public function activate(Request $request, $kode)
    {
        $SkemaGrosir = SkemaGrosir::find($kode);
        $data = array(
            'flag_aktif' => 'Y',
        );

        $status = $SkemaGrosir->update($data);
        message($status, 'Skema Grosir Berhasil Diaktifkan Kembali', 'Skema Grosir Gagal Diaktifkan Kembali');
        return redirect('/skema-grosir');
    }

    public function deactivate(Request $request, $kode)
    {

        $SkemaGrosir = SkemaGrosir::find($kode);
        $data = array('flag_aktif' => 'N',);

        $status = $SkemaGrosir->update($data);
        message($status, 'Skema Grosir Berhasil Dinonaktifkan', 'Skema Grosir Gagal Dinonaktifkan');

        return redirect('/skema-grosir');
    }
}
