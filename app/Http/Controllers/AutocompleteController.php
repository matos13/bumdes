<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Models;
use DB;

use Illuminate\Support\Facades\Auth;

class AutocompleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function search($method,Request $r)
    {
        return $this->$method($r);
    }

    //autocomplete goes here
  	 private function permission($r){
        $q=$r->input("q");
        $query=Models\Permission::select(\DB::raw("permissions.*"))
        ->where("name","like","%$q%")
        ->limit(20);
        $results=$query->get();
        return \Response::json($results->toArray());
      }
	     private function parent($r){
          $q=$r->input("q");
          $query=Models\Menu::select(\DB::raw("menus.*"))
          ->where("name","like","%$q%")
          ->limit(20);
          $results=$query->get();
          return \Response::json($results->toArray());
        }
    private function role($r){
      $q=strtoupper($r->input("q"));
      $query=\App\Role::select(\DB::raw("*"))
      ->where("name","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function provinsi($r){
      $q=strtoupper($r->input("q"));
      $query=Models\Provinsi::select(\DB::raw("*"))
      ->where("provinsi","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function kabupaten($r){
      $q=strtoupper($r->input("q"));
      $query=Models\Kabupaten::select(\DB::raw("*"))
      ->where("kabupaten","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function kecamatan($r){
      $q=strtoupper($r->input("q"));
      $query=Models\Kecamatan::select(\DB::raw("*"))
      ->where("kecamatan","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function kelurahan($r){
      $q=strtoupper($r->input("q"));
      $query=Models\Kelurahan::select(\DB::raw("*"))
      ->where("kelurahan","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function grosir($r){
      $q=strtoupper($r->input("q"));
      $query=Models\Grosir::select(\DB::raw("*"))
      ->where("grosir","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function kategori_barang($r){
      $q=strtoupper($r->input("q"));
      $query=Models\KategoriBarang::select(\DB::raw("*"))
      ->where("kategori_barang","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function sub_kategori_barang($r){
      $q=strtoupper($r->input("q"));
      $query=Models\SubKategoriBarang::select(\DB::raw("*"))
      ->where("sub_kategori_barang","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function barang($r){
      $id_grosir=Auth::user()->id_grosir;
      $q=strtoupper($r->input("q"));
      if ($id_grosir) {
       $query=Models\Barang::select(\DB::raw("*"))
        ->where("barang","like","%$q%")
        ->where('id_grosir',$id_grosir)
        ->limit(20);
      }else{
        $query=Models\Barang::select(\DB::raw("*"))
        ->where("barang","like","%$q%")
        ->limit(20);
      }

      $results=$query->get();
      return \Response::json($results->toArray());
    }
    private function bumdes($r){
      $q=strtoupper($r->input("q"));
      $query=Models\Bumde::select(\DB::raw("*"))
      ->where("bumdes","like","%$q%")
      ->limit(20);
      $results=$query->get();
      return \Response::json($results->toArray());
    }
  

}
