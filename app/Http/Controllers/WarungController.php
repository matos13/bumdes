<?php

namespace App\Http\Controllers;

use App\Models\Warung;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserApk;
use Datatables;
use Illuminate\Support\Facades\DB;

class WarungController extends Controller
{
    public $viewDir = "warung";
    public $breadcrumbs = array(
        'permissions' => array('title' => 'Warung', 'link' => "#", 'active' => false, 'display' => true),
    );

    public function __construct()
    {
        $this->middleware('permission:read-warung');
    }

    public function index()
    {
        return $this->view("index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->view("form", ['warung' => new Warung]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {

            $date = date_create($request->all()['tgl_lahir']);
            $auto_pass = str_replace("-", "", date_format($date, 'd-m-Y'));
            $options   = [
                'cost' => 12,
            ];
            $password_hashed = password_hash($auto_pass, PASSWORD_BCRYPT, $options);

            $cek_no_hp = UserApk::select('*')->where('username',$request->all()['no_hp'])->get();
            // dd($cek_no_hp);
            if ($cek_no_hp) {
                message(false, '', 'No Telp telah di daftarakan, gunakan No Telp lain');
                return redirect('warung');
            }
            $apk['username'] = $request->all()['no_hp'];
            $apk['nohp'] = $request->all()['no_hp'];
            $apk['id_kelurahan'] = $request->all()['id_kelurahan'];
            $apk['status'] = 'Grosir';
            $apk['id_firebase'] = 'null';
            $apk['password'] = $password_hashed;
            $apk['status_login'] = 'Warung';

            $user_apk = UserApk::create($apk);
            if ($user_apk) {
                $insert['warung'] = $request->all()['warung'];
                $insert['alamat'] = $request->all()['alamat'];
                $insert['id_grosir'] = $request->all()['id_grosir'];
                $insert['id_kelurahan'] = $request->all()['id_kelurahan'];
                $insert['nama_ktp'] = $request->all()['nama_ktp'];
                $insert['nik'] = $request->all()['nik'];
                $insert['jenis_kelamin'] = $request->all()['jenis_kelamin'];
                $insert['tempat_lahir'] = $request->all()['tempat_lahir'];
                $insert['tgl_lahir'] = $request->all()['tgl_lahir'];
                $insert['alamat_ktp'] = $request->all()['alamat_ktp'];
                $insert['nama_pemilik'] = $request->all()['nama_pemilik'];
                $insert['alamat_pemilik'] = $request->all()['alamat_pemilik'];
                $insert['no_hp'] = $request->all()['no_hp'];
                $insert['id_user_apk'] = $user_apk->id;

                if ($files = $request->file('ktp')) {
                    $destinationPath = 'public/images/ktp'; // upload path
                    $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
                    $files->move($destinationPath, $profileImage);
                    $insert['ktp'] = "$profileImage";
                }
                $act = Warung::create($insert);
            }
            DB::commit();
            message($act, 'Data Warung berhasil ditambahkan', 'Data Warung gagal ditambahkan');
        } catch (\Throwable $th) {
            dd($th);
            DB::rollBack();
        }
        return redirect('warung');
    }

    /**
     * Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function show(Request $request, $kode)
    {
        $warung = Warung::find($kode);
        return $this->view("show", ['warung' => $warung]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function edit(Request $request, $kode)
    {
        $warung = Warung::find($kode);
        return $this->view("form", ['warung' => $warung]);
    }
    public function detail(Request $request, $kode)
    {
        $warung = Warung::find($kode);
        return $this->view("detail", ['warung' => $warung]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        $warung = Warung::find($kode);
        try {
            $user_apk = UserApk::find($warung->id_user_apk);
            if ($user_apk) {
                $apk['username'] = $request->all()['no_hp'];
                $apk['nohp'] = $request->all()['no_hp'];
                $apk['id_kelurahan'] = $request->all()['id_kelurahan'];
                $user_apk->update($apk);
            } else {
                $date = date_create($request->all()['tgl_lahir']);
                $auto_pass = str_replace("-", "", date_format($date, 'd-m-Y'));
                $options   = [
                    'cost' => 12,
                ];
                $password_hashed = password_hash($auto_pass, PASSWORD_BCRYPT, $options);
                $apk['username'] = $request->all()['no_hp'];
                $apk['nohp'] = $request->all()['no_hp'];
                $apk['id_kelurahan'] = $request->all()['id_kelurahan'];
                $apk['status'] = 'Grosir';
                $apk['id_firebase'] = 'null';
                $apk['password'] = $password_hashed;
                $apk['status_login'] = 'Warung';
                $user_apk = UserApk::create($apk);
            }

            $insert['warung'] = $request->all()['warung'];
            $insert['alamat'] = $request->all()['alamat'];
            $insert['id_grosir'] = $request->all()['id_grosir'];
            $insert['id_kelurahan'] = $request->all()['id_kelurahan'];
            $insert['nama_ktp'] = $request->all()['nama_ktp'];
            $insert['nik'] = $request->all()['nik'];
            $insert['jenis_kelamin'] = $request->all()['jenis_kelamin'];
            $insert['tempat_lahir'] = $request->all()['tempat_lahir'];
            $insert['tgl_lahir'] = $request->all()['tgl_lahir'];
            $insert['alamat_ktp'] = $request->all()['alamat_ktp'];
            $insert['nama_pemilik'] = $request->all()['nama_pemilik'];
            $insert['alamat_pemilik'] = $request->all()['alamat_pemilik'];
            $insert['no_hp'] = $request->all()['no_hp'];
            $insert['id_user_apk'] = $user_apk->id;

            if ($files = $request->file('ktp')) {
                $destinationPath = 'public/images/ktp'; // upload path
                $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $insert['ktp'] = "$profileImage";
            }

            $act = $warung->update($insert);
            DB::commit();
            message($act, 'Data Warung berhasil diupdate', 'Data Warung gagal diupdate');
        } catch (\Throwable $th) {
            dd($th);
            DB::rollBack();
        }
        return redirect('/warung');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return  \Illuminate\Http\Response
     */
    public function destroy(Request $request, $kode)
    {
        $warung = Warung::find($kode);
        $act = false;
        try {
            $act = $warung->forceDelete();
        } catch (\Exception $e) {
            $warung = Warung::find($warung->pk());
            $act = $warung->delete();
        }
    }

    protected function view($view, $data = [])
    {
        return view($this->viewDir . "." . $view, $data);
    }
    public function loadData()
    {
        $GLOBALS['nomor'] = \Request::input('start', 1) + 1;
        $dataList = Warung::select('*');
        if (request()->get('status') == 'trash') {
            $dataList->onlyTrashed();
        }
        return Datatables::of($dataList)
            ->addColumn('nomor', function ($kategori) {
                return $GLOBALS['nomor']++;
            })
            ->addColumn('status', function ($data) {
                if (isset($data->flag_aktif)) {
                    return array('id' => $data->pk(), 'flag_aktif' => $data->flag_aktif);
                } else {
                    return null;
                }
            })
            ->addColumn('action', function ($data) {
                $edit = url("warung/" . $data->pk()) . "/edit";
                $detail = url("warung/" . $data->pk()) . "/detail";
                $delete = url("warung/" . $data->pk());
                $content = '';
                $content .= "<a onclick='show_modal(\"$detail\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Detail'><i class='icon md-eye' aria-hidden='true'></i></a>";
                $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                return $content;
            })
            ->make(true);
    }
    public function activate(Request $request, $kode)
    {
        $warung = Warung::find($kode);
        $data = array(
            'flag_aktif' => 'Y',
        );

        $status = $warung->update($data);
        message($status, 'Warung Berhasil Diaktifkan Kembali', 'Warung Gagal Diaktifkan Kembali');
        return redirect('/warung');
    }

    public function deactivate(Request $request, $kode)
    {

        $warung = Warung::find($kode);
        $data = array('flag_aktif' => 'N',);

        $status = $warung->update($data);
        message($status, 'Warung Berhasil Dinonaktifkan', 'Warung Gagal Dinonaktifkan');

        return redirect('/warung');
    }
}
