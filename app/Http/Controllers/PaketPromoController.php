<?php
namespace App\Http\Controllers;

use App\Models\PaketPromo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class PaketPromoController extends Controller
{
    public $viewDir = "paket_promo";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Paket-promo','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-paket-promo');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['paketPromo' => new PaketPromo]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {    
        //    dd($request->all());
            $tgl_awal=date_create($request->all()['tanggal_awal']);
            $tgl_akhir=date_create($request->all()['tanggal_akhir']);
            $this->validate($request, PaketPromo::validationRules());
            $insert['paket']        = $request->all()['paket'];
            $insert['tanggal_awal'] = date_format($tgl_awal,'Y-m-d') ;
            $insert['tanggal_akhir']= date_format($tgl_akhir,'Y-m-d') ;
            $insert['total_harga']  = str_replace(".", "",$request->all()['total_harga']);
            $insert['status_aktif'] = $request->all()['status_aktif'];
            if ($files = $request->file('gambar')) {
                $destinationPath = 'public/images/promo'; // upload path
                $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $insert['gambar'] = "$profileImage";
              }

           $act=PaketPromo::create($insert);
           message($act,'Data Paket Promo berhasil ditambahkan','Data Paket Promo gagal ditambahkan');
           return redirect('paket-promo');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $paketPromo=PaketPromo::find($kode);
           return $this->view("show",['paketPromo' => $paketPromo]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $paketPromo=PaketPromo::find($kode);
           return $this->view( "form", ['paketPromo' => $paketPromo] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $paketPromo=PaketPromo::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, PaketPromo::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $paketPromo->update($data);
               return "Record updated";
           }
           $tgl_awal=date_create($request->all()['tanggal_awal']);
           $tgl_akhir=date_create($request->all()['tanggal_akhir']);
           $this->validate($request, PaketPromo::validationRules());

           $insert['paket']        = $request->all()['paket'];
           $insert['tanggal_awal'] = date_format($tgl_awal,'Y-m-d') ;
           $insert['tanggal_akhir']= date_format($tgl_akhir,'Y-m-d') ;
           $insert['total_harga']  = str_replace(".", "",$request->all()['total_harga']);
           $insert['status_aktif'] = $request->all()['status_aktif'];
           if ($files = $request->file('gambar')) {
               $destinationPath = 'public/images/promo'; // upload path
               $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
               $files->move($destinationPath, $profileImage);
               $insert['gambar'] = "$profileImage";
             }

           $act=$paketPromo->update($insert);
           message($act,'Data Paket Promo berhasil diupdate','Data Paket Promo gagal diupdate');

           return redirect('/paket-promo');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $paketPromo=PaketPromo::find($kode);
           $act=false;
           try {
               $act=$paketPromo->forceDelete();
           } catch (\Exception $e) {
               $paketPromo=PaketPromo::find($paketPromo->pk());
               $act=$paketPromo->delete();
           }
       }
    public function activate(Request $request, $kode)
     {
      $paket= PaketPromo::find($kode);
      $data=array(
        'status_aktif'=>'Y',
      );

      $status=$paket->update($data);
      message($status,'Paket Promo Berhasil Diaktifkan Kembali','Paket Promo Gagal Diaktifkan Kembali');
      return redirect('/paket-promo');
    } 

    public function deactivate(Request $request, $kode){

      $paket=PaketPromo::find($kode);
      $data=array('status_aktif'=>'N',);

      $status=$paket->update($data);
      message($status,'Paket Promo Berhasil Dinonaktifkan','PAket Promo Gagal Dinonaktifkan');

      return redirect('/paket-promo');
    }
       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = PaketPromo::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('total_harga',function($data){
                return rupiahTanpaKoma($data->total_harga);
              })
               ->addColumn('tanggal_awal',function($data){
                return date_format(date_create($data->tanggal_awal),'d/m/Y') ;
              })
               ->addColumn('tanggal_akhir',function($data){
                return date_format(date_create($data->tanggal_akhir),'d/m/Y') ;
              })
              ->addColumn('status', function ($data) {
                if(isset($data->status_aktif))
                {
                 return array('id'=>$data->pk(),'status_aktif'=>$data->status_aktif);
               }else
               {
                 return null;
               }
             })
             ->addColumn('detail', function ($data) {
                return array('id'=>$data->pk());
             })
               ->addColumn('action', function ($data) {
                   $edit=url("paket-promo/".$data->pk())."/edit";
                   $delete=url("paket-promo/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
       public function detail(Request $request, $kode)
       {
        $paket=PaketPromo::find($kode);
        return $this->view( "detail", ['paketPromo' => $paket] );
       }       
}
