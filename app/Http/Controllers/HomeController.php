<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Datetime;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use App\Models\Notification;

date_default_timezone_set("Asia/Jakarta");

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()       
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

   
    public function index()
    {
        $id_grosir=Auth::user()->id_grosir;
        if ($id_grosir) {
            $barang = DB::table('barang')->where('id_grosir',$id_grosir)->count();
            $warung = DB::table('warung')->where('id_grosir',$id_grosir)->count();
        }else{
            $barang = DB::table('barang')->count();
            $warung = DB::table('warung')->count();
        }
       return view('home',["barang"=>$barang,"warung"=>$barang]);
   }

}
