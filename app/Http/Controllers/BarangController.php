<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use Illuminate\Support\Facades\Auth;

class BarangController extends Controller
{
  public $viewDir = "barang";
  public $breadcrumbs = array(
    'permissions' => array('title' => 'Barang', 'link' => "#", 'active' => false, 'display' => true),
  );

  public function __construct()
  {
    $this->middleware('permission:read-barang');
    //    $id_grosir=
  }

  public function index()
  {
    return $this->view("index");
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return  \Illuminate\Http\Response
   */
  public function create()
  {
    //    dd();
    $id_grosir = Auth::user()->id_grosir;
    return $this->view("form", ['barang' => new Barang, 'id_grosir' => $id_grosir]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param    \Illuminate\Http\Request  $request
   * @return  \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, Barang::validationRules());
    $insert['barang']                = $request->all()['barang'];
    $insert['id_grosir']             = $request->all()['id_grosir'];
    $insert['id_sub_kategori_barang'] = $request->all()['id_sub_kategori_barang'];
    $insert['harga_normal']          = str_replace(".", "", $request->all()['harga_normal']);
    $insert['flag_aktif']            = $request->all()['flag_aktif'];
    $insert['stok']                  = $request->all()['stok'];
    if ($files = $request->file('gambar')) {
      $destinationPath = 'public/images/barang'; // upload path
      $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
      $files->move($destinationPath, $profileImage);
      $insert['gambar'] = "$profileImage";
    }
    $act = Barang::create($insert);
    message($act, 'Data Barang berhasil ditambahkan', 'Data Barang gagal ditambahkan');
    return redirect('barang');
  }

  /**
   * Display the specified resource.
   *
   * @return  \Illuminate\Http\Response
   */
  public function show(Request $request, $kode)
  {
    $barang = Barang::find($kode);

    return $this->view("show", ['barang' => $barang]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @return  \Illuminate\Http\Response
   */
  public function edit(Request $request, $kode)
  {
    $barang = Barang::find($kode);
    $id_grosir = Auth::user()->id_grosir;
    return $this->view("form", ['barang' => $barang, 'id_grosir' => $id_grosir]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param    \Illuminate\Http\Request  $request
   * @return  \Illuminate\Http\Response
   */
  public function update(Request $request, $kode)
  {
    $barang = Barang::find($kode);
    if ($request->isXmlHttpRequest()) {
      $data = [$request->name  => $request->value];
      $validator = \Validator::make($data, Barang::validationRules($request->name));
      if ($validator->fails())
        return response($validator->errors()->first($request->name), 403);
      $barang->update($data);
      return "Record updated";
    }
    $this->validate($request, Barang::validationRules());
    $insert['barang']                = $request->all()['barang'];
    $insert['id_grosir']             = $request->all()['id_grosir'];
    $insert['id_sub_kategori_barang'] = $request->all()['id_sub_kategori_barang'];
    $insert['harga_normal']          = str_replace(".", "", $request->all()['harga_normal']);
    $insert['flag_aktif']            = $request->all()['flag_aktif'];
    $insert['stok']                  = $request->all()['stok'];
    if ($files = $request->file('gambar')) {
      $destinationPath = 'public/images/barang'; // upload path
      $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
      $files->move($destinationPath, $profileImage);
      $insert['gambar'] = "$profileImage";
    }
    $act = $barang->update($insert);
    message($act, 'Data Barang berhasil diupdate', 'Data Barang gagal diupdate');

    return redirect('/barang');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @return  \Illuminate\Http\Response
   */
  public function destroy(Request $request, $kode)
  {
    $barang = Barang::find($kode);
    $act = false;
    try {
      $act = $barang->forceDelete();
    } catch (\Exception $e) {
      $barang = Barang::find($barang->pk());
      $act = $barang->delete();
    }
  }

  protected function view($view, $data = [])
  {
    return view($this->viewDir . "." . $view, $data);
  }

  public function loadData()
  {
    $GLOBALS['nomor'] = \Request::input('start', 1) + 1;
    $id_grosir = Auth::user()->id_grosir;
    if ($id_grosir) {
      $dataList = Barang::select('*')->where('id_grosir', $id_grosir);
    } else {
      $dataList = Barang::select('*');
    }
    if (request()->get('status') == 'trash') {
      $dataList->onlyTrashed();
    }
    return Datatables::of($dataList)
      ->addColumn('nomor', function ($kategori) {
        return $GLOBALS['nomor']++;
      })
      ->addColumn('status', function ($data) {
        if ($data->flag_aktif == 'Y') {
          $status = 'Aktif';
        } else {
          $status = 'Tidak Aktif';
        }
        return $status;
      })
      ->addColumn('harga_normal', function ($data) {
        return rupiahTanpaKoma($data->harga_normal);
      })
      ->addColumn('grosir', function ($data) {
        return $data->grosir->grosir;
      })
      ->addColumn('sub_kategori_barang', function ($data) {
        //  dd($data->sub_kategori_barang);
        return $data->sub_kategori_barang->sub_kategori_barang;
      })
      ->addColumn('status', function ($data) {
        if (isset($data->flag_aktif)) {
          return array('id' => $data->pk(), 'flag_aktif' => $data->flag_aktif);
        } else {
          return null;
        }
      })
      ->addColumn('action', function ($data) {
        $edit = url("barang/" . $data->pk()) . "/edit";
        $delete = url("barang/" . $data->pk());
        $content = '';
        $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
        $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

        return $content;
      })
      ->make(true);
  }
  public function activate(Request $request, $kode)
  {
    $Barang = Barang::find($kode);
    $data = array(
      'flag_aktif' => 'Y',
    );

    $status = $Barang->update($data);
    message($status, 'Barang Berhasil Diaktifkan Kembali', 'Barang Gagal Diaktifkan Kembali');
    return redirect('/barang');
  }

  public function deactivate(Request $request, $kode)
  {

    $Barang = Barang::find($kode);
    $data = array('flag_aktif' => 'N',);

    $status = $Barang->update($data);
    message($status, 'Barang Berhasil Dinonaktifkan', 'Barang Gagal Dinonaktifkan');

    return redirect('/barang');
  }
}
