<?php
namespace App\Http\Controllers;

use App\Models\LogOrder;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class LogOrderController extends Controller
{
    public $viewDir = "log_order";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Log-order','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-log-order');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['logOrder' => new LogOrder]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, LogOrder::validationRules());

           $act=LogOrder::create($request->all());
           message($act,'Data Log Order berhasil ditambahkan','Data Log Order gagal ditambahkan');
           return redirect('log-order');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $logOrder=LogOrder::find($kode);
           return $this->view("show",['logOrder' => $logOrder]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $logOrder=LogOrder::find($kode);
           return $this->view( "form", ['logOrder' => $logOrder] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $logOrder=LogOrder::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, LogOrder::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $logOrder->update($data);
               return "Record updated";
           }
           $this->validate($request, LogOrder::validationRules());

           $act=$logOrder->update($request->all());
           message($act,'Data Log Order berhasil diupdate','Data Log Order gagal diupdate');

           return redirect('/log-order');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $logOrder=LogOrder::find($kode);
           $act=false;
           try {
               $act=$logOrder->forceDelete();
           } catch (\Exception $e) {
               $logOrder=LogOrder::find($logOrder->pk());
               $act=$logOrder->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = LogOrder::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("log-order/".$data->pk())."/edit";
                   $delete=url("log-order/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
         }
