<?php
namespace App\Http\Controllers;

use App\Models\Bumde;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class BumdeController extends Controller
{
    public $viewDir = "bumde";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Bumdes','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-bumdes');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['bumde' => new Bumde]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
        //    dd($request->all());
           $this->validate($request, Bumde::validationRules());

           $act=Bumde::create($request->all());
           message($act,'Data Bumdes berhasil ditambahkan','Data Bumdes gagal ditambahkan');
           return redirect('bumde');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $bumde=Bumde::find($kode);
           return $this->view("show",['bumde' => $bumde]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $bumde=Bumde::find($kode);
           return $this->view( "form", ['bumde' => $bumde] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $bumde=Bumde::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, Bumde::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $bumde->update($data);
               return "Record updated";
           }
           $this->validate($request, Bumde::validationRules());

           $act=$bumde->update($request->all());
           message($act,'Data Bumdes berhasil diupdate','Data Bumdes gagal diupdate');

           return redirect('/bumde');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $bumde=Bumde::find($kode);
           $act=false;
           try {
               $act=$bumde->forceDelete();
           } catch (\Exception $e) {
               $bumde=Bumde::find($bumde->pk());
               $act=$bumde->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = Bumde::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('status', function ($data) {
                //    dd($data->status_aktif);
                if(isset($data->status_aktif))
                {
                 return array('id'=>$data->pk(),'status_aktif'=>$data->status_aktif);
                }else
                {
                 return null;
                }
                
            })
            ->addColumn('kelurahan',function($data){
                return $data->kelurahan->kelurahan;
            })
               ->addColumn('action', function ($data) {
                   $edit=url("bumde/".$data->pk())."/edit";
                   $delete=url("bumde/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
       public function activate(Request $request, $kode)
       {
        // dd($kode);
            $kategoriBarang= Bumde::find($kode);
            $data=array(
            'status_aktif'=>'Y',
            );
            
            $status=$kategoriBarang->update($data);
            message($status,'Data Bumdes Berhasil Diaktifkan Kembali','Data Bumdes Gagal Diaktifkan Kembali');
            return redirect('/bumde');
       } 

        public function deactivate(Request $request, $kode){
        
            $kategoriBarang=Bumde::find($kode);
            $data=array('status_aktif'=>'N',);
       
            $status=$kategoriBarang->update($data);
            message($status,'Data Bumdes Berhasil Dinonaktifkan','Data Bumdes Gagal Dinonaktifkan');
        
            return redirect('/bumde');
       }
}
