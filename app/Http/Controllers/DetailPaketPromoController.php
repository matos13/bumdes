<?php
namespace App\Http\Controllers;

use App\Models\DetailPaketPromo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class DetailPaketPromoController extends Controller
{
    public $viewDir = "detail_paket_promo";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Detail-paket-promo','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-detail-paket-promo');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create(Request $request)
       {
        //    dd($request);
           return $this->view("form",['detailPaketPromo' => new DetailPaketPromo]);
       }
       public function buat(Request $request,$kode)
       {
        //    dd($kode);
           return $this->view("form",['detailPaketPromo' => new DetailPaketPromo,'id_paket_promo' => $kode]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, DetailPaketPromo::validationRules());

           $tgl_awal=date_create($request->all()['tanggal_awal']);
           $tgl_akhir=date_create($request->all()['tanggal_akhir']);

           $insert = array(
                'tanggal_awal'      => $tgl_awal,
                'tanggal_akhir'     => $tgl_awal,
                'status_aktif'      => $request->all()['status_aktif'],
                'id_paket_promo'    => $request->all()['id_paket_promo'],
                'id_barang'         => $request->all()['id_barang'],
             );
           $act=DetailPaketPromo::create($insert);
           message($act,'Data Detail Paket Promo berhasil ditambahkan','Data Detail Paket Promo gagal ditambahkan');
           return redirect('paket-promo/detail/'.$request->all()['id_paket_promo']);
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $detailPaketPromo=DetailPaketPromo::find($kode);
           return $this->view("show",['detailPaketPromo' => $detailPaketPromo]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $detailPaketPromo=DetailPaketPromo::find($kode);
        //    dd($detailPaketPromo);
           return $this->view( "form", ['detailPaketPromo' => $detailPaketPromo,'id_paket_promo'=>$detailPaketPromo->id_paket_promo] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $detailPaketPromo=DetailPaketPromo::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, DetailPaketPromo::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $detailPaketPromo->update($data);
               return "Record updated";
           }
           $this->validate($request, DetailPaketPromo::validationRules());
           $tgl_awal=date_create($request->all()['tanggal_awal']);
           $tgl_akhir=date_create($request->all()['tanggal_akhir']);

           $insert = array(
                'tanggal_awal'      => $tgl_awal,
                'tanggal_akhir'     => $tgl_awal,
                'status_aktif'      => $request->all()['status_aktif'],
                'id_paket_promo'    => $request->all()['id_paket_promo'],
                'id_barang'         => $request->all()['id_barang'],
             );
           $act=$detailPaketPromo->update($insert);
           message($act,'Data Detail Paket Promo berhasil diupdate','Data Detail Paket Promo gagal diupdate');

           return redirect('paket-promo/detail/'.$request->all()['id_paket_promo']);
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $detailPaketPromo=DetailPaketPromo::find($kode);
           $act=false;
           try {
               $act=$detailPaketPromo->forceDelete();
           } catch (\Exception $e) {
               $detailPaketPromo=DetailPaketPromo::find($detailPaketPromo->pk());
               $act=$detailPaketPromo->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData(Request $request, $kode)
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = DetailPaketPromo::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('action', function ($data) {
                   $edit=url("detail-paket-promo/".$data->pk())."/edit";
                   $delete=url("detail-paket-promo/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
       public function loadData2(Request $request, $kode)
       {
        //    dd($kode);
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = DetailPaketPromo::select('*')->where('id_paket_promo',$kode);
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('tanggal_awal',function($data){
                return date_format(date_create($data->tanggal_awal),'d/m/Y') ;
              })
               ->addColumn('tanggal_akhir',function($data){
                return date_format(date_create($data->tanggal_akhir),'d/m/Y') ;
              })
              ->addColumn('barang',function($data){
                return $data->barang->barang;
             })
              ->addColumn('paket_promo',function($data){
                return $data->paket_promo->paket;
             })
             ->addColumn('status', function ($data) {
                if(isset($data->status_aktif))
                {
                 return array('id'=>$data->pk(),'status_aktif'=>$data->status_aktif);
               }else
               {
                 return null;
               }
             })
               ->addColumn('action', function ($data) {
                   $edit=url("detail-paket-promo/".$data->pk())."/edit";
                   $delete=url("detail-paket-promo/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
       public function activate(Request $request, $kode)
     {
      $paket= DetailPaketPromo::find($kode);
      $data=array(
        'status_aktif'=>'Y',
      );

      $status=$paket->update($data);
      message($status,'Data Berhasil Diaktifkan Kembali','Data Gagal Diaktifkan Kembali');
      return redirect('paket-promo/detail/'.$paket->id_paket_promo);
    } 

    public function deactivate(Request $request, $kode){
      $paket=DetailPaketPromo::find($kode);
      $data=array('status_aktif'=>'N',);

      $status=$paket->update($data);
      message($status,'Data Berhasil Dinonaktifkan','Data Gagal Dinonaktifkan');

      return redirect('paket-promo/detail/'.$paket->id_paket_promo);
    }
}
