<?php
namespace App\Http\Controllers;

use App\Models\SubKategoriBarang;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class SubKategoriBarangController extends Controller
{
    public $viewDir = "sub_kategori_barang";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'Sub-kategori-barang','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-sub-kategori-barang');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['subKategoriBarang' => new SubKategoriBarang]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, SubKategoriBarang::validationRules());
           $insert['id_kategori_barang']=$request->all()['id_kategori_barang'];
           $insert['sub_kategori_barang']=$request->all()['sub_kategori_barang'];
           $insert['flag_aktif']=$request->all()['flag_aktif'];
           if ($files = $request->file('icon')) {
                $destinationPath = 'public/images/sub-kategori-barang-icon'; // upload path
                $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $insert['icon'] = "$profileImage";
            }
           $act=SubKategoriBarang::create($insert);
           message($act,'Data Sub Kategori Barang berhasil ditambahkan','Data Sub Kategori Barang gagal ditambahkan');
           return redirect('sub-kategori-barang');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $subKategoriBarang=SubKategoriBarang::find($kode);
           return $this->view("show",['subKategoriBarang' => $subKategoriBarang]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $subKategoriBarang=SubKategoriBarang::find($kode);
           return $this->view( "form", ['subKategoriBarang' => $subKategoriBarang] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $subKategoriBarang=SubKategoriBarang::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, SubKategoriBarang::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $subKategoriBarang->update($data);
               return "Record updated";
           }
           $this->validate($request, SubKategoriBarang::validationRules());
           $insert['id_kategori_barang']=$request->all()['id_kategori_barang'];
           $insert['sub_kategori_barang']=$request->all()['sub_kategori_barang'];
           $insert['flag_aktif']=$request->all()['flag_aktif'];
           if ($files = $request->file('icon')) {
                $destinationPath = 'public/images/sub-kategori-barang-icon'; // upload path
                $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
                $files->move($destinationPath, $profileImage);
                $insert['icon'] = "$profileImage";
            }
           $act=$subKategoriBarang->update($insert);
           message($act,'Data Sub Kategori Barang berhasil diupdate','Data Sub Kategori Barang gagal diupdate');

           return redirect('/sub-kategori-barang');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $subKategoriBarang=SubKategoriBarang::find($kode);
           $act=false;
           try {
               $act=$subKategoriBarang->forceDelete();
           } catch (\Exception $e) {
               $subKategoriBarang=SubKategoriBarang::find($subKategoriBarang->pk());
               $act=$subKategoriBarang->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
           $dataList = SubKategoriBarang::select('*');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
               ->addColumn('status', function ($data) {
                if(isset($data->flag_aktif))
                {
                 return array('id'=>$data->pk(),'flag_aktif'=>$data->flag_aktif);
                }else
                {
                 return null;
                }
                
            })
            ->addColumn('kategori_barang',function($data){
                return $data->kategori_barang->kategori_barang;
            })
               ->addColumn('action', function ($data) {
                   $edit=url("sub-kategori-barang/".$data->pk())."/edit";
                   $delete=url("sub-kategori-barang/".$data->pk());
                 $content = '';
                  $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
                  $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

                   return $content;
               })
               ->make(true);
       }
       public function activate(Request $request, $kode)
       {
        // dd($kode);
            $subKategoriBarang= SubKategoriBarang::find($kode);
            $data=array(
            'flag_aktif'=>'Y',
            );
            
            $status=$subKategoriBarang->update($data);
            message($status,'Sub Kategori Barang Berhasil Diaktifkan Kembali','Kategori Barang Gagal Diaktifkan Kembali');
            return redirect('/sub-kategori-barang');
       } 

        public function deactivate(Request $request, $kode){
        
            $subKategoriBarang=SubKategoriBarang::find($kode);
            $data=array('flag_aktif'=>'N',);
       
            $status=$subKategoriBarang->update($data);
            message($status,'Sub Kategori Barang Berhasil Dinonaktifkan','Kategori Barang Gagal Dinonaktifkan');
        
            return redirect('/sub-kategori-barang');
       }
}
