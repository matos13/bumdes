<?php
namespace App\Http\Controllers;

use App\Models\UserApk;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;

class UserApkController extends Controller
{
    public $viewDir = "user_apk";
    public $breadcrumbs = array(
         'permissions'=>array('title'=>'User-apk','link'=>"#",'active'=>false,'display'=>true),
       );

       public function __construct()
       {
           $this->middleware('permission:read-user-apk');
       }

       public function index()
       {
           return $this->view( "index");
       }

       /**
        * Show the form for creating a new resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function create()
       {
           return $this->view("form",['userApk' => new UserApk]);
       }

       /**
        * Store a newly created resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function store( Request $request )
       {
           $this->validate($request, UserApk::validationRules());

           $act=UserApk::create($request->all());
           message($act,'Data User Apk berhasil ditambahkan','Data User Apk gagal ditambahkan');
           return redirect('user-apk');
       }

       /**
        * Display the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function show(Request $request, $kode)
       {
           $userApk=UserApk::find($kode);
           return $this->view("show",['userApk' => $userApk]);
       }

       /**
        * Show the form for editing the specified resource.
        *
        * @return  \Illuminate\Http\Response
        */
       public function edit(Request $request, $kode)
       {
           $userApk=UserApk::find($kode);
           return $this->view( "form", ['userApk' => $userApk] );
       }

       /**
        * Update the specified resource in storage.
        *
        * @param    \Illuminate\Http\Request  $request
        * @return  \Illuminate\Http\Response
        */
       public function update(Request $request, $kode)
       {
           $userApk=UserApk::find($kode);
           if( $request->isXmlHttpRequest() )
           {
               $data = [$request->name  => $request->value];
               $validator = \Validator::make( $data, UserApk::validationRules( $request->name ) );
               if($validator->fails())
                   return response($validator->errors()->first( $request->name),403);
               $userApk->update($data);
               return "Record updated";
           }
           $this->validate($request, UserApk::validationRules());

           $act=$userApk->update($request->all());
           message($act,'Data User Apk berhasil diupdate','Data User Apk gagal diupdate');

           return redirect('/user-apk');
       }

       /**
        * Remove the specified resource from storage.
        *
        * @return  \Illuminate\Http\Response
        */
       public function destroy(Request $request, $kode)
       {
           $userApk=UserApk::find($kode);
           $act=false;
           try {
               $act=$userApk->forceDelete();
           } catch (\Exception $e) {
               $userApk=UserApk::find($userApk->pk());
               $act=$userApk->delete();
           }
       }

       protected function view($view, $data = [])
       {
           return view($this->viewDir.".".$view, $data);
       }
       public function loadData()
       {
           $GLOBALS['nomor']=\Request::input('start',1)+1;
        //    $dataList = UserApk::select('*');
           $dataList = UserApk::select('user_apk.*','data_diri.jenis_kelamin','data_diri.nama_pemilik','data_diri.alamat_pemilik')->join('data_diri','user_apk.id','=','data_diri.id_user_apk');
           if (request()->get('status') == 'trash') {
               $dataList->onlyTrashed();
           }
           return Datatables::of($dataList)
               ->addColumn('nomor',function($kategori){
                   return $GLOBALS['nomor']++;
               })
            //    ->addColumn('action', function ($data) {
            //        $edit=url("user-apk/".$data->pk())."/edit";
            //        $delete=url("user-apk/".$data->pk());
            //      $content = '';
            //       $content .= "<a onclick='show_modal(\"$edit\")' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row ' data-toggle='tooltip' data-original-title='Edit'><i class='icon md-edit' aria-hidden='true'></i></a>";
            //       $content .= " <a onclick='hapus(\"$delete\")' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row' data-toggle='tooltip' data-original-title='Remove'><i class='icon md-delete' aria-hidden='true'></i></a>";

            //        return $content;
            //    })
               ->make(true);
       }
         }
