<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class DetailOrder extends Model {
    use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="detail_order";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = DetailOrder::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('id_order') and $query->where('id_order',\Request::input('id_order'));
        \Request::input('id_barang') and $query->where('id_barang',\Request::input('id_barang'));
        \Request::input('qty') and $query->where('qty',\Request::input('qty'));
        \Request::input('harga') and $query->where('harga',\Request::input('harga'));
        \Request::input('subtotal') and $query->where('subtotal',\Request::input('subtotal'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'id_order' => 'integer',
            'id_barang' => 'integer',
            'qty' => 'integer',
            'harga' => 'integer',
            'subtotal' => 'integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function order()
    {
        return $this->belongsTo('App\Models\Order','id_order');
    }

    public function setOrderAttribute($order) {
      unset($this->attributes['order']);
    }

    
    
    public function barang()
    {
        return $this->belongsTo('App\Models\Barang','id_barang');
    }

    public function setBarangAttribute($barang) {
      unset($this->attributes['barang']);
    }

    
    }
