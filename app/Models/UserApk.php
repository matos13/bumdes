<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class UserApk extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="user_apk";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = UserApk::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('username') and $query->where('username','like','%'.\Request::input('username').'%');
        \Request::input('nohp') and $query->where('nohp','like','%'.\Request::input('nohp').'%');
        \Request::input('id_kelurahan') and $query->where('id_kelurahan',\Request::input('id_kelurahan'));
        \Request::input('status') and $query->where('status','like','%'.\Request::input('status').'%');
        \Request::input('password') and $query->where('password','like','%'.\Request::input('password').'%');
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'username' => 'string|max:100',
            'nohp' => 'string|max:15',
            'id_kelurahan' => 'integer',
            'status' => 'required|string|max:10',
            'password' => 'required|string|max:191',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function kelurahan()
    {
        return $this->belongsTo('App\Models\Kelurahan','id_kelurahan');
    }

    public function setKelurahanAttribute($kelurahan) {
      unset($this->attributes['kelurahan']);
    }

    
    }
