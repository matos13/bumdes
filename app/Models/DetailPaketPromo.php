<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class DetailPaketPromo extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="detail_paket_promo";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = DetailPaketPromo::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('tanggal_awal') and $query->where('tanggal_awal',\Request::input('tanggal_awal'));
        \Request::input('tanggal_akhir') and $query->where('tanggal_akhir',\Request::input('tanggal_akhir'));
        \Request::input('status_aktif') and $query->where('status_aktif','like','%'.\Request::input('status_aktif').'%');
        \Request::input('id_paket_promo') and $query->where('id_paket_promo',\Request::input('id_paket_promo'));
        \Request::input('id_barang') and $query->where('id_barang',\Request::input('id_barang'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'tanggal_awal' => '',
            'tanggal_akhir' => '',
            'status_aktif' => 'required|string|max:1',
            'id_paket_promo' => 'integer',
            'id_barang' => 'integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function paket_promo()
    {
        return $this->belongsTo('App\Models\PaketPromo','id_paket_promo');
    }

    public function setPaket_promoAttribute($paket_promo) {
      unset($this->attributes['paket_promo']);
    }

    
    
    public function barang()
    {
        return $this->belongsTo('App\Models\Barang','id_barang');
    }

    public function setBarangAttribute($barang) {
      unset($this->attributes['barang']);
    }

    
    }
