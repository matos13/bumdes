<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class Barang extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="barang";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = Barang::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('barang') and $query->where('barang','like','%'.\Request::input('barang').'%');
        \Request::input('id_grosir') and $query->where('id_grosir',\Request::input('id_grosir'));
        \Request::input('id_sub_kategori_barang') and $query->where('id_sub_kategori_barang',\Request::input('id_kategori_barang'));
        \Request::input('harga_normal') and $query->where('harga_normal',\Request::input('harga_normal'));
        \Request::input('flag_aktif') and $query->where('flag_aktif','like','%'.\Request::input('flag_aktif').'%');
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'barang' => 'string|max:255',
            'id_grosir' => 'integer',
            'id_sub_kategori_barang' => 'integer',
            'harga_normal' => 'required',
            'flag_aktif' => 'required|string|max:1',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function grosir()
    {
        return $this->belongsTo('App\Models\Grosir','id_grosir');
    }

    public function setGrosirAttribute($grosir) {
      unset($this->attributes['grosir']);
    }

    
    
    public function sub_kategori_barang()
    {
        return $this->belongsTo('App\Models\SubKategoriBarang','id_sub_kategori_barang');
    }

    public function setSub_kategori_barangAttribute($sub_kategori_barang) {
      unset($this->attributes['sub_kategori_barang']);
    }

    
    }
