<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class OrderDatum extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="order_data";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = OrderDatum::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('id_user_apk') and $query->where('id_user_apk',\Request::input('id_user_apk'));
        \Request::input('status_order') and $query->where('status_order','like','%'.\Request::input('status_order').'%');
        \Request::input('tanggal_order') and $query->where('tanggal_order',\Request::input('tanggal_order'));
        \Request::input('latitude') and $query->where('latitude','like','%'.\Request::input('latitude').'%');
        \Request::input('longitude') and $query->where('longitude','like','%'.\Request::input('longitude').'%');
        \Request::input('total') and $query->where('total',\Request::input('total'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'id_user_apk' => 'integer',
            'status_order' => 'string|max:100',
            'tanggal_order' => '',
            'latitude' => 'string|max:100',
            'longitude' => 'string|max:100',
            'total' => 'integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function user_apk()
    {
        return $this->belongsTo('App\Models\User_apk','id_user_apk');
    }

    public function setUser_apkAttribute($user_apk) {
      unset($this->attributes['user_apk']);
    }

    
    }
