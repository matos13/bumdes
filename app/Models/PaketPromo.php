<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class PaketPromo extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="paket_promo";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = PaketPromo::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('paket') and $query->where('paket','like','%'.\Request::input('paket').'%');
        \Request::input('gambar') and $query->where('gambar','like','%'.\Request::input('gambar').'%');
        \Request::input('tanggal_awal') and $query->where('tanggal_awal',\Request::input('tanggal_awal'));
        \Request::input('tanggal_akhir') and $query->where('tanggal_akhir',\Request::input('tanggal_akhir'));
        \Request::input('status_aktif') and $query->where('status_aktif','like','%'.\Request::input('status_aktif').'%');
        \Request::input('total_harga') and $query->where('total_harga',\Request::input('total_harga'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'paket' => 'required|string|max:225',
            'tanggal_awal' => '',
            'tanggal_akhir' => '',
            'status_aktif' => 'required|string|max:1',
            'total_harga' => 'required',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    }
