<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Traits\RecordSignature;

class Warung extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="warung";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = Warung::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('warung') and $query->where('warung','like','%'.\Request::input('warung').'%');
        \Request::input('alamat') and $query->where('alamat','like','%'.\Request::input('alamat').'%');
        \Request::input('id_grosir') and $query->where('id_grosir',\Request::input('id_grosir'));
        \Request::input('id_kelurahan') and $query->where('id_kelurahan',\Request::input('id_kelurahan'));
        \Request::input('flag_aktif') and $query->where('flag_aktif','like','%'.\Request::input('flag_aktif').'%');
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        \Request::input('nama_ktp') and $query->where('nama_ktp','like','%'.\Request::input('nama_ktp').'%');
        \Request::input('nik') and $query->where('nik','like','%'.\Request::input('nik').'%');
        \Request::input('jenis_kelamin') and $query->where('jenis_kelamin','like','%'.\Request::input('jenis_kelamin').'%');
        \Request::input('tempat_lahir') and $query->where('tempat_lahir','like','%'.\Request::input('tempat_lahir').'%');
        \Request::input('tgl_lahir') and $query->where('tgl_lahir',\Request::input('tgl_lahir'));
        \Request::input('alamat_ktp') and $query->where('alamat_ktp','like','%'.\Request::input('alamat_ktp').'%');
        \Request::input('nama_pemilik') and $query->where('nama_pemilik','like','%'.\Request::input('nama_pemilik').'%');
        \Request::input('alamat_pemilik') and $query->where('alamat_pemilik','like','%'.\Request::input('alamat_pemilik').'%');
        \Request::input('ktp') and $query->where('ktp','like','%'.\Request::input('ktp').'%');
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'warung' => 'string|max:255',
            'alamat' => 'string',
            'id_grosir' => 'integer',
            'id_kelurahan' => 'integer',
            'flag_aktif' => 'required|string|max:1',
            'nama_ktp' => 'string|max:255',
            'nik' => 'string|max:20',
            'jenis_kelamin' => 'string|max:1',
            'tempat_lahir' => 'string|max:100',
            'tgl_lahir' => 'date',
            'alamat_ktp' => 'string',
            'nama_pemilik' => 'string|max:255',
            'alamat_pemilik' => 'string',
            'ktp' => 'string|max:255',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function grosir()
    {
        return $this->belongsTo('App\Models\Grosir','id_grosir');
    }

    public function setGrosirAttribute($grosir) {
      unset($this->attributes['grosir']);
    }

    
    
    public function kelurahan()
    {
        return $this->belongsTo('App\Models\Kelurahan','id_kelurahan');
    }

    public function setKelurahanAttribute($kelurahan) {
      unset($this->attributes['kelurahan']);
    }

    
    }
