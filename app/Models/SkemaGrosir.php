<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class SkemaGrosir extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="skema_grosir";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = SkemaGrosir::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('id_barang') and $query->where('id_barang',\Request::input('id_barang'));
        \Request::input('jumlah_minimal') and $query->where('jumlah_minimal',\Request::input('jumlah_minimal'));
        \Request::input('jumlah_maksimal') and $query->where('jumlah_maksimal',\Request::input('jumlah_maksimal'));
        \Request::input('harga_perbarang') and $query->where('harga_perbarang',\Request::input('harga_perbarang'));
        \Request::input('flag_aktif') and $query->where('flag_aktif','like','%'.\Request::input('flag_aktif').'%');
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'id_barang' => 'integer',
            'jumlah_minimal' => 'integer',
            'jumlah_maksimal' => 'integer',
            'harga_perbarang' => 'required',
            'flag_aktif' => 'required|string|max:1',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function barang()
    {
        return $this->belongsTo('App\Models\Barang','id_barang');
    }

    public function setBarangAttribute($barang) {
      unset($this->attributes['barang']);
    }

    
    }
