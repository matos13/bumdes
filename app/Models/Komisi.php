<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class Komisi extends Model {
    // use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="komisi";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = Komisi::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('id_bumdes') and $query->where('id_bumdes',\Request::input('id_bumdes'));
        \Request::input('total_belanja') and $query->where('total_belanja',\Request::input('total_belanja'));
        \Request::input('presentase_komisi') and $query->where('presentase_komisi',\Request::input('presentase_komisi'));
        \Request::input('total_komisi') and $query->where('total_komisi',\Request::input('total_komisi'));
        \Request::input('tanggal_komisi') and $query->where('tanggal_komisi',\Request::input('tanggal_komisi'));
        \Request::input('status_bayar') and $query->where('status_bayar','like','%'.\Request::input('status_bayar').'%');
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'id_bumdes' => 'integer',
            'total_belanja' => 'required',
            'presentase_komisi' => 'required',
            'total_komisi' => 'required',
            'tanggal_komisi' => '',
            'status_bayar' => 'required|string|max:1',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function bumdes()
    {
        return $this->belongsTo('App\Models\Bumdes','id_bumdes');
    }

    public function setBumdesAttribute($bumdes) {
      unset($this->attributes['bumdes']);
    }

    
    }
