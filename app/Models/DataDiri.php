<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\RecordSignature;

class DataDiri extends Model {
    use RecordSignature;
    
    public $guarded = ["id","created_at","updated_at"];
    protected $table="data_diri";
    public $timestamps=true;
    protected $primaryKey = "id";
    public $incrementing = true;
    public static function findRequested()
    {
        $query = DataDiri::query();

        // search results based on user input
        \Request::input('id') and $query->where('id',\Request::input('id'));
        \Request::input('id_user_apk') and $query->where('id_user_apk',\Request::input('id_user_apk'));
        \Request::input('nik') and $query->where('nik','like','%'.\Request::input('nik').'%');
        \Request::input('jenis_kelamin') and $query->where('jenis_kelamin','like','%'.\Request::input('jenis_kelamin').'%');
        \Request::input('tempat_lahir') and $query->where('tempat_lahir','like','%'.\Request::input('tempat_lahir').'%');
        \Request::input('tanggal_lahir') and $query->where('tanggal_lahir',\Request::input('tanggal_lahir'));
        \Request::input('alamat_pemilik') and $query->where('alamat_pemilik','like','%'.\Request::input('alamat_pemilik').'%');
        \Request::input('nama_pemilik') and $query->where('nama_pemilik','like','%'.\Request::input('nama_pemilik').'%');
        \Request::input('foto_ktp') and $query->where('foto_ktp','like','%'.\Request::input('foto_ktp').'%');
        \Request::input('id_warung') and $query->where('id_warung',\Request::input('id_warung'));
        \Request::input('created_at') and $query->where('created_at',\Request::input('created_at'));
        \Request::input('updated_at') and $query->where('updated_at',\Request::input('updated_at'));
        
        // sort results
        \Request::input("sort") and $query->orderBy(\Request::input("sort"),\Request::input("sortType","asc"));

        // paginate results
        return $query->paginate(15);
    }

    public static function validationRules( $attributes = null )
    {
        $rules = [
            'id_user_apk' => 'integer',
            'nik' => 'string|max:100',
            'jenis_kelamin' => 'string|max:100',
            'tempat_lahir' => 'string|max:100',
            'tanggal_lahir' => 'date',
            'alamat_pemilik' => 'string',
            'nama_pemilik' => 'string|max:100',
            'foto_ktp' => 'string',
            'id_warung' => 'integer',
        ];

        // no list is provided
        if(!$attributes)
            return $rules;

        // a single attribute is provided
        if(!is_array($attributes))
            return [ $attributes => $rules[$attributes] ];

        // a list of attributes is provided
        $newRules = [];
        foreach ( $attributes as $attr )
            $newRules[$attr] = $rules[$attr];
        return $newRules;
    }

    public function pk(){
      return $this->{$this->primaryKey};
    }

    
    public function user_apk()
    {
        return $this->belongsTo('App\Models\User_apk','id_user_apk');
    }

    public function setUser_apkAttribute($user_apk) {
      unset($this->attributes['user_apk']);
    }

    
    
    public function warung()
    {
        return $this->belongsTo('App\Models\Warung','id_warung');
    }

    public function setWarungAttribute($warung) {
      unset($this->attributes['warung']);
    }

    
    }
