<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
Route::get('register/create-resend/{user}', ['as' => 'register.create_resend', 'uses' => 'Auth\RegisterController@createResend']);
Route::post('register/resend', ['as' => 'register.resend', 'uses' => 'Auth\RegisterController@resend']);

Route::get('/send', 'SendMessageController@index')->name('send');
Route::get('/sendEmailNotifikasi', 'SendMessageController@sendEmailNotifikasi');
Route::post('/postMessage', 'SendMessageController@sendMessage')->name('postMessage');

Route::get('/', function () {
    // return view('welcome');
    if (Auth::check()) {

        return redirect('beranda');
    } else {

        return redirect('login');
    }
});

Route::get('/admin', function () {
    // return view('welcome');
    if (Auth::check()) {

        return redirect('beranda');
    } else {

        return redirect('login');
    }
});

Auth::routes();


Route::get('/beranda', 'HomeController@index')->name('beranda');
Route::match(['get', 'post'], 'home/get-notif', 'HomeController@getNotif');
Route::match(['get', 'post'], 'home/update-notif', 'HomeController@updateNotif');
Route::match(['get', 'post'], 'home/delete-notif', 'HomeController@deleteNotif');

Route::get('user/load-data', 'UserController@loadData');
Route::get('user/json', 'UserController@json');
Route::get('user/activate/{id}', 'UserController@activate');
Route::get('user/update/{id}', 'UserController@update');
Route::get('user/deactivate/{id}', 'UserController@deactivate');
Route::get('user/{id}/reset', 'UserController@reset');
Route::get('user/cek-username-type1', 'UserController@cekusername');
Route::get('user/cek-email-type1', 'UserController@cekemail');
Route::resource('user', 'UserController');
Route::delete('user/{id}/restore', 'UserController@restore');

Route::get('autocomplete/{method}', 'AutocompleteController@search');

Route::post('role/createpermissionrole', ['as' => 'role.createpermissionrole', 'uses' => 'RoleController@createpermissionrole']);

Route::get('role/load-data', 'RoleController@loadData');
Route::get('permission-role/get/{id}/menu', 'RoleController@hakmenus');
Route::get('role/permission-role/get/{id}/menu', 'RoleController@hakmenus');
Route::resource('role', 'RoleController');
Route::delete('role/{id}/restore', 'RoleController@restore');

Route::get('permission-role/load-data', 'PermissionRoleController@loadData');
Route::resource('permission-role', 'PermissionRoleController');
Route::delete('permission-role/{id}/restore', 'PermissionRoleController@restore');

Route::get('menu/load-data', 'MenuController@loadData');
Route::resource('menu', 'MenuController');
Route::delete('menu/{id}/restore', 'MenuController@restore');

Route::get('permission/load-data', 'PermissionController@loadData');
Route::resource('permission', 'PermissionController');
Route::delete('permission/{id}/restore', 'PermissionController@restore');

Route::get('/pengaturan', 'ConfigIdController@settings')->name('settings');
Route::get('/delete-logo', 'ConfigIdController@deleteLogo');
Route::get('config-id/load-data', 'ConfigIdController@loadData');
Route::match(['get', 'post'], 'config-id/upload-foto', 'ConfigIdController@uploadFoto');
Route::match(['get', 'post'], 'config-id/check-username', 'ConfigIdController@checkUsername');
Route::match(['get', 'post'], 'config-id/check-email', 'ConfigIdController@checkEmail');
Route::match(['get', 'post'], 'config-id/check-password', 'ConfigIdController@checkPassword');
Route::get('config-id/delete-foto', 'ConfigIdController@deleteFoto');
Route::resource('config-id', 'ConfigIdController');
Route::delete('config-id/{id}/restore', 'ConfigIdController@restore');

Route::get('activity/load-data', 'ActivityLogController@loadData');
Route::get('activity/get-data', 'ActivityLogController@getData');
Route::resource('activity', 'ActivityLogController');
Route::delete('activity/{id}/restore', 'ActivityLogController@restore');

Route::get('provinsi/load-data', 'ProvinsiController@loadData');
Route::resource('provinsi', 'ProvinsiController');
Route::delete('provinsi/{id}/restore', 'ProvinsiController@restore');

Route::get('kabupaten/load-data', 'KabupatenController@loadData');
Route::resource('kabupaten', 'KabupatenController');
Route::delete('kabupaten/{id}/restore', 'KabupatenController@restore');

Route::get('kecamatan/load-data', 'KecamatanController@loadData');
Route::resource('kecamatan', 'KecamatanController');
Route::delete('kecamatan/{id}/restore', 'KecamatanController@restore');

Route::get('kelurahan/load-data', 'KelurahanController@loadData');
Route::resource('kelurahan', 'KelurahanController');
Route::delete('kelurahan/{id}/restore', 'KelurahanController@restore');

Route::get('kategori-barang/load-data', 'KategoriBarangController@loadData');
Route::resource('kategori-barang', 'KategoriBarangController');
Route::delete('kategori-barang/{id}/restore', 'KategoriBarangController@restore');
Route::get('kategori-barang/deactivate/{id}', 'KategoriBarangController@deactivate');
Route::get('kategori-barang/activate/{id}',   'KategoriBarangController@activate');

Route::get('grosir/load-data', 'GrosirController@loadData');
Route::resource('grosir', 'GrosirController');
Route::delete('grosir/{id}/restore', 'GrosirController@restore');

Route::get('barang/load-data', 'BarangController@loadData');
Route::resource('barang', 'BarangController');
Route::delete('barang/{id}/restore', 'BarangController@restore');
Route::get('barang/deactivate/{id}', 'BarangController@deactivate');
Route::get('barang/activate/{id}',   'BarangController@activate');

Route::get('skema-grosir/load-data', 'SkemaGrosirController@loadData');
Route::resource('skema-grosir', 'SkemaGrosirController');
Route::delete('skema-grosir/{id}/restore', 'SkemaGrosirController@restore');
Route::get('skema-grosir/deactivate/{id}', 'SkemaGrosirController@deactivate');
Route::get('skema-grosir/activate/{id}',   'SkemaGrosirController@activate');

Route::get('warung/load-data', 'WarungController@loadData');
Route::resource('warung', 'WarungController');
Route::delete('warung/{id}/restore', 'WarungController@restore');
Route::get('warung/deactivate/{id}', 'WarungController@deactivate');
Route::get('warung/activate/{id}',   'WarungController@activate');

Route::get('user-apk/load-data', 'UserApkController@loadData');
Route::resource('user-apk', 'UserApkController');
Route::delete('user-apk/{id}/restore', 'UserApkController@restore');

Route::get('data-diri/load-data', 'DataDiriController@loadData');
Route::resource('data-diri', 'DataDiriController');
Route::delete('data-diri/{id}/restore', 'DataDiriController@restore');

Route::get('order-datum/load-data', 'OrderDatumController@loadData');
Route::get('order-datum/detail/{id}',   'OrderDatumController@detail');
Route::get('order-datum/cetak/{id}',   'OrderDatumController@cetak');
Route::get('order-datum/confirm/{id}',   'OrderDatumController@confirm');
Route::resource('order-datum', 'OrderDatumController');
Route::delete('order-datum/{id}/restore', 'OrderDatumController@restore');

Route::get('detail-order/load-data', 'DetailOrderController@loadData');
Route::resource('detail-order', 'DetailOrderController');
Route::delete('detail-order/{id}/restore', 'DetailOrderController@restore');

Route::get('sub-kategori-barang/load-data', 'SubKategoriBarangController@loadData');
Route::resource('sub-kategori-barang', 'SubKategoriBarangController');
Route::delete('sub-kategori-barang/{id}/restore', 'SubKategoriBarangController@restore');
Route::get('sub-kategori-barang/deactivate/{id}', 'SubKategoriBarangController@deactivate');
Route::get('sub-kategori-barang/activate/{id}',   'SubKategoriBarangController@activate');

Route::get('user-apk/load-data', 'UserApkController@loadData');
Route::resource('user-apk', 'UserApkController');
Route::delete('user-apk/{id}/restore', 'UserApkController@restore');

Route::get('log-order/load-data', 'LogOrderController@loadData');
Route::resource('log-order', 'LogOrderController');
Route::delete('log-order/{id}/restore', 'LogOrderController@restore');

Route::get('minimal-order/load-data', 'MinimalOrderController@loadData');
Route::resource('minimal-order', 'MinimalOrderController');
Route::delete('minimal-order/{id}/restore', 'MinimalOrderController@restore');

Route::get('bumde/load-data', 'BumdeController@loadData');
Route::resource('bumde', 'BumdeController');
Route::delete('bumde/{id}/restore', 'BumdeController@restore');

Route::get('bumde/load-data', 'BumdeController@loadData');
Route::resource('bumde', 'BumdeController');
Route::delete('bumde/{id}/restore', 'BumdeController@restore');
Route::get('bumde/deactivate/{id}', 'BumdeController@deactivate');
Route::get('bumde/activate/{id}',   'BumdeController@activate');


Route::get('komisi/load-data', 'KomisiController@loadData');
Route::resource('komisi', 'KomisiController');
Route::delete('komisi/{id}/restore', 'KomisiController@restore');

Route::get('presentase-komisi/load-data', 'PresentaseKomisiController@loadData');
Route::resource('presentase-komisi', 'PresentaseKomisiController');
Route::delete('presentase-komisi/{id}/restore', 'PresentaseKomisiController@restore');

Route::get('paket-promo/load-data', 'PaketPromoController@loadData');
Route::resource('paket-promo', 'PaketPromoController');
Route::delete('paket-promo/{id}/restore', 'PaketPromoController@restore');
Route::get('paket-promo/deactivate/{id}', 'PaketPromoController@deactivate');
Route::get('paket-promo/activate/{id}',   'PaketPromoController@activate');
Route::get('paket-promo/detail/{id}',   'PaketPromoController@detail');

Route::get('detail-paket-promo/load-data', 'DetailPaketPromoController@loadData');
Route::resource('detail-paket-promo', 'DetailPaketPromoController');
Route::get('detail-paket-promo/load/{id}', 'DetailPaketPromoController@loadData2');
Route::delete('detail-paket-promo/{id}/restore', 'DetailPaketPromoController@restore');
Route::get('detail-paket-promo/deactivate/{id}', 'DetailPaketPromoController@deactivate');
Route::get('detail-paket-promo/activate/{id}',   'DetailPaketPromoController@activate');
Route::get('detail-paket-promo/buat/{id}',   'DetailPaketPromoController@buat');

Route::get('pembayaran/load-data', 'PembayaranController@loadData');
Route::get('pembayaran/load-data2', 'PembayaranController@loadData2');
Route::get('pembayaran/detail/{id}', 'PembayaranController@detail');
Route::get('pembayaran/simpan/{id}', 'PembayaranController@simpan');
Route::get('pembayaran/cetak/{id}', 'PembayaranController@cetak');
Route::get('pembayaran/info/', 'PembayaranController@info');
Route::resource('pembayaran', 'PembayaranController');
Route::delete('pembayaran/{id}/restore', 'PembayaranController@restore');

Route::get('warung/load-data','WarungController@loadData');
Route::resource('warung','WarungController');
Route::delete('warung/{id}/restore','WarungController@restore');
Route::get('warung/deactivate/{id}', 'WarungController@deactivate');
Route::get('warung/activate/{id}',   'WarungController@activate');
Route::get('warung/{id}/detail',   'WarungController@detail');
