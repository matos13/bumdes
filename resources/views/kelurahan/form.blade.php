<div class="modal-dialog modal-simple">

	{{ Form::model($kelurahan,array('route' => array((!$kelurahan->exists) ? 'kelurahan.store':'kelurahan.update',$kelurahan->pk()),
	'class'=>'modal-content','id'=>'kelurahan-form','method'=>(!$kelurahan->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($kelurahan->exists?'Edit':'Tambah').' Kelurahan' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('kode_pos','text')->model($kelurahan)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('kelurahan','text')->model($kelurahan)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('id_kecamatan','hidden')->model($kelurahan)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('kecamatan',array('value'=>$kelurahan->exists?(isset($kelurahan->kecamatan)?$kelurahan->kecamatan->kecamatan:null):null))->model(null)->show() !!}
		{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($kelurahan)->show() !!}  --}}
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($kelurahan->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#kelurahan-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				flag_aktif : { validators: {
					notEmpty: {
						message: 'Kolom flag_aktif tidak boleh kosong'
					}
				}
			}
		},
		err: {
			clazz: 'invalid-feedback'
		},
		control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var kecamatanEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/kecamatan") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#kecamatan").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: kecamatanEngine.ttAdapter(),
			name: "kecamatan",
			displayKey: "kecamatan",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{kecamatan}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>kecamatan tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_kecamatan").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			

		});
	</script>
