<div class="modal-dialog modal-lg modal-simple">
    {{ Form::model($warung, ['route' => [!$warung->exists ? 'warung.store' : 'warung.update', $warung->pk()], 'class' => 'modal-content', 'id' => 'warung-form', 'method' => !$warung->exists ? 'POST' : 'PUT', 'enctype' => 'multipart/form-data']) }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="formModalLabel">{{ ($warung->exists ? 'Edit' : 'Tambah Data') . ' Warung' }}</h4>
    </div>
    <div class="modal-body">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <table style="width: 100%">
                        <tr>
                            <td>Warung</td>
                            <td>:</td>
                            <td>{{ $warung->warung }}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>:</td>
                            <td>{{ $warung->alamat }}</td>
                        </tr>
                        <tr>
                            <td>Kelurahan</td>
                            <td>:</td>
                            <td>{{ $warung->Kelurahan->kelurahan }}</td>
                        </tr>
                        <tr>
                            <td>Grosir</td>
                            <td>:</td>
                            <td>{{ $warung->grosir->grosir }}</td>
                        </tr>
                        <tr>
                            <td>Nama KTP</td>
                            <td>:</td>
                            <td>{{ $warung->nama_ktp }}</td>
                        </tr>
                        <tr>
                            <td>NIK</td>
                            <td>:</td>
                            <td>{{ $warung->nik }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Kelamin</td>
                            <td>:</td>
                            <td>{{ $warung->jenis_kelamin }}</td>
                        </tr>
                        <tr>
                            <td>NO HP</td>
                            <td>:</td>
                            <td>{{ $warung->no_hp }}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm">
                    <table style="width: 100%">
                        <tr>
                            <td>Tempat Lahir</td>
                            <td>:</td>
                            <td>{{ $warung->tempat_lahir }}</td>
                        </tr>
                        <tr>
                            <td>Tgl Lahir</td>
                            <td>:</td>
                            <td>{{ $warung->tgl_lahir }}</td>
                        </tr>
                        <tr>
                            <td>Alamat KTP</td>
                            <td>:</td>
                            <td>{{ $warung->alamat_ktp }}</td>
                        </tr>
                        <tr>
                            <td>Nama Pemilik</td>
                            <td>:</td>
                            <td>{{ $warung->nama_pemilik }}</td>
                        </tr>
                        <tr>
                            <td>Alamat Pemilik</td>
                            <td>:</td>
                            <td>{{ $warung->alamat_pemilik }}</td>
                        </tr>
                        <tr>
                            <td>KTP</td>
                            <td>:</td>
                            <td><a href="{{ url('/').'/public/images/ktp/'.$warung->ktp }}" target="_blank"
                                    rel="noopener noreferrer">{{ $warung->ktp }}</a></td>
                        </tr>
                    </table>
                </div>
            </div>
            {{-- <div class="col-md-12 float-right">
                <div class="text-right">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">Tutup</button>
                </div>
            </div> --}}
        </div>
        {{ Form::close() }}
    </div>
</div>
