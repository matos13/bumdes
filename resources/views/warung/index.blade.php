@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1 class="page-title">Warung</h1>
        @include('layouts.inc.breadcrumb')
        <div class="page-header-actions">
            <a class="btn btn-block btn-primary data-modal" id="data-modal" href="#"
                onclick="show_modal('{{ route('warung.create') }}')">Tambah</a>
        </div>
    </div>
    <div class="page-content">
        <!-- Panel Table Tools -->
        <div class="panel">
            <header class="panel-heading">
                <div class="form-group col-md-12">
                    <div class="form-group">
                        <!-- <h3 class="panel-title">Table Tools</h3> -->

                    </div>
                </div>
            </header>
            <div class="panel-body">
                <!-- <table class="table table-bordered" id="users-table">

                             </table> -->
                <table class="table table-hover dataTable table-striped w-full" id="warung-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Warung</th>
                            <th>Alamat</th>
                            {{-- <th>id_grosir</th>
                            <th>id_kelurahan</th>
                            <th>flag_aktif</th>
                            <th>created_at</th>
                            <th>nama_ktp</th>
                            <th>nik</th>
                            <th>jenis_kelamin</th>
                            <th>tempat_lahir</th>
                            <th>tgl_lahir</th>
                            <th>alamat_ktp</th> --}}
                            <th>Nama Pemilik</th>
                            <th>Alamat Pemilik</th>
                            <th>Status</th>
                            {{-- <th>ktp</th> --}}
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- End Panel Table Tools -->
    </div>
    <div class="modal fade" id="formModal" aria-hidden="true" aria-labelledby="formModalLabel" role="dialog" tabindex="-1">
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function() {
            $('.trash-ck').click(function() {
                if ($('.trash-ck').prop('checked')) {
                    document.location = '{{ url('warung?status=trash') }}';
                } else {
                    document.location = '{{ url('warung') }}';
                }
            });
            $('#warung-table').DataTable({
                stateSave: true,
                processing: true,
                serverSide: true,
                pageLength: 20,
                ajax: {
                    url: "{{ url('warung/load-data') }}",
                    data: function(d) {

                    }
                },
                columns: [{
                        data: 'nomor',
                        name: 'nomor',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'warung',
                        name: 'warung'
                    },
                    {
                        data: 'alamat',
                        name: 'alamat'
                    },
                    // {
                    //     data: 'id_grosir',
                    //     name: 'id_grosir'
                    // },
                    // {
                    //     data: 'id_kelurahan',
                    //     name: 'id_kelurahan'
                    // },
                    // {
                    //     data: 'flag_aktif',
                    //     name: 'flag_aktif'
                    // },
                    // {
                    //     data: 'created_at',
                    //     name: 'created_at'
                    // },
                    // {
                    //     data: 'nama_ktp',
                    //     name: 'nama_ktp'
                    // },
                    // {
                    //     data: 'nik',
                    //     name: 'nik'
                    // },
                    // {
                    //     data: 'jenis_kelamin',
                    //     name: 'jenis_kelamin'
                    // },
                    // {
                    //     data: 'tempat_lahir',
                    //     name: 'tempat_lahir'
                    // },
                    // {
                    //     data: 'tgl_lahir',
                    //     name: 'tgl_lahir'
                    // },
                    // {
                    //     data: 'alamat_ktp',
                    //     name: 'alamat_ktp'
                    // },
                    {
                        data: 'nama_pemilik',
                        name: 'nama_pemilik'
                    },
                    {
                        data: 'alamat_pemilik',
                        name: 'alamat_pemilik'
                    },
                    // {
                    //     data: 'ktp',
                    //     name: 'ktp'
                    // },
                    {
                        data: 'status',
                        name: 'Status',
                        searchable: false,
                        orderable: false,
                        "render": function(data, type, row) {
                            if (data.flag_aktif == 'Y') {
                                return '<a class="btn btn-success btn-xs"  style="color:white; font-family:Arial" title="Nonaktifkan " onclick="con(\'' +
                                    data.id + '\',\'' + data.flag_aktif + '\') ">Aktif</a>';
                            } else {
                                return '<a class="btn btn-danger btn-xs"  style="color:white;font-family:Arial" title="Aktifkan" onclick="con(\'' +
                                    data.id + '\',\'' + data.flag_aktif + '\') ">Tidak Aktif</a>';
                            }
                        }
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                language: {
                    lengthMenu: '{{ 'Menampilkan _MENU_ data' }}',
                    zeroRecords: '{{ 'Data tidak ditemukan' }}',
                    info: '{{ '_PAGE_ dari _PAGES_ halaman' }}',
                    infoEmpty: '{{ 'Data tidak ditemukan' }}',
                    infoFiltered: '{{ '(Penyaringan dari _MAX_ data)' }}',
                    loadingRecords: '{{ 'Memuat data dari server' }}',
                    processing: '{{ 'Memuat data data' }}',
                    search: '{{ 'Pencarian:' }}',
                    paginate: {
                        first: '{{ '<' }}',
                        last: '{{ '>' }}',
                        next: '{{ '>>' }}',
                        previous: '{{ '<<' }}'
                    }
                },
                aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [-1]
                }],
                iDisplayLength: 5,
                aLengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                // sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
                // buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
            });
        });

        function con(id, status) {
            if (status == 'Y') {
                swal({
                    title: "Apakah anda yakin?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false
                }, function(isConfirm) {
                    if (!isConfirm) return;
                    window.location.href = "{{ url('warung/deactivate/') }}/" + id + "";
                });
            } else {
                swal({
                    title: "Apakah anda yakin?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false
                }, function(isConfirm) {
                    if (!isConfirm) return;
                    window.location.href = "{{ url('warung/activate/') }}/" + id + "";
                });
            }

        }
    </script>
@endpush
