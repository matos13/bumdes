<div class="modal-dialog modal-lg modal-simple">

    {{ Form::model($warung, ['route' => [!$warung->exists ? 'warung.store' : 'warung.update', $warung->pk()], 'class' => 'modal-content', 'id' => 'warung-form', 'method' => !$warung->exists ? 'POST' : 'PUT', 'enctype' => 'multipart/form-data']) }}

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="formModalLabel">{{ ($warung->exists ? 'Edit' : 'Tambah Data') . ' Warung' }}
        </h4>
    </div>
    <div class="modal-body">
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    {!! App\Console\Commands\Generator\Form::input('warung', 'text')->model($warung)->show() !!}
                    {!! App\Console\Commands\Generator\Form::textarea('alamat')->model($warung)->show() !!}
                    {!! App\Console\Commands\Generator\Form::input('id_grosir', 'hidden')->model($warung)->showHidden() !!}
                    {!! App\Console\Commands\Generator\Form::autocomplete('grosir', ['value' => $warung->exists ? (isset($warung->grosir) ? $warung->grosir->grosir : null) : null])->model(null)->show() !!}
                    {!! App\Console\Commands\Generator\Form::input('id_kelurahan', 'hidden')->model($warung)->showHidden() !!}
                    {!! App\Console\Commands\Generator\Form::autocomplete('kelurahan', ['value' => $warung->exists ? (isset($warung->kelurahan) ? $warung->kelurahan->kelurahan : null) : null])->model(null)->show() !!}
                    {!! App\Console\Commands\Generator\Form::input('nama_ktp', 'text')->model($warung)->show() !!}
                    {!! App\Console\Commands\Generator\Form::input('nik', 'number')->model($warung)->show() !!}
                    {{-- {!! App\Console\Commands\Generator\Form::input('jenis_kelamin', 'text')->model($warung)->show() !!} --}}
                    <?php
                    $jk = $warung->jenis_kelamin;
                    $ktp = $warung->ktp;
                    ?>
                    <div class="form-group row">
                        <label for="warung" class="col-form-label col-md-3">Jenis Kelamin</label>
                        <div class="col-md-7">
                            <div class="radio-custom radio-primary">
                                <input type="radio" id="laki" name="jenis_kelamin" value="L" <?php echo $jk == 'L' ? 'checked' : null; ?>>
                                <label for="laki">Laki-Laki</label>
                            </div>
                            <div class="radio-custom radio-primary">
                                <input type="radio" id="perempuan" name="jenis_kelamin" value="P" <?php echo $jk == 'P' ? 'checked' : null; ?>>
                                <label for="perempuan">Perempuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="no_hp" class="col-form-label col-md-3">No HP</label>
                        <div class="col-md-7">
                            <input name="no_hp" id="no_hp" class="form-control" type="number"
                                value="{{ $warung->no_hp }}">
                        </div>
                    </div>
                </div>
                <div class="col-sm">
                    {{-- {!! App\Console\Commands\Generator\Form::input('flag_aktif', 'text')->model($warung)->show() !!} --}}

                    {!! App\Console\Commands\Generator\Form::input('tempat_lahir', 'text')->model($warung)->show() !!}
                    {!! App\Console\Commands\Generator\Form::input('tgl_lahir', 'date')->model($warung)->show() !!}
                    {!! App\Console\Commands\Generator\Form::textarea('alamat_ktp')->model($warung)->show() !!}
                    {!! App\Console\Commands\Generator\Form::input('nama_pemilik', 'text')->model($warung)->show() !!}
                    {!! App\Console\Commands\Generator\Form::textarea('alamat_pemilik')->model($warung)->show() !!}
                    {{-- {!! App\Console\Commands\Generator\Form::input('ktp', 'text')->model($warung)->show() !!} --}}
                    <div class="form-group row">
                        <label for="kategori_barang" class="col-form-label col-md-3">KTP</label>
                        {{-- <div class="col-md-7">
                            <input type="file" id="ktp" name="ktp" class="form-control" placeholder=""
                                accept="image/jpeg">
                        </div> --}}
                        <div class="custom-file">
                            <input type="file" class="form-control" id="ktp" name="ktp" style="width: 37%;" />
                            <label class="custom-file-label" for="inputGroupFile02"><?php echo $ktp; ?></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 float-right">
            <div class="text-right">
                <button class="btn btn-primary" id="simpan">Simpan</button>
            </div>
        </div>
    </div>

    {{ Form::close() }}
</div>


<script type="text/javascript">
    $('#ktp').on('change', function() {
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })

    function isValid(value) {
        var fieldNum =/^[A-Za-z\s]+$/;

        if ((value.match(fieldNum))) {
            return true
        } else {
            return false
        }

    }
    $(document).ready(function() {
        $('#warung-form').formValidation({
            framework: "bootstrap4",
            button: {
                selector: "#simpan",
                disabled: "disabled"
            },
            icon: null,
            fields: {
                no_hp: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom No HP tidak boleh kosong'
                        }
                    }
                },
                warung: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Warung tidak boleh kosong'
                        }
                    }
                },
                alamat: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Alamat tidak boleh kosong'
                        }
                    }
                },
                // grosir: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Kolom Grosir tidak boleh kosong'
                //         }
                //     }
                // },
                // kelurahan: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Kolom Kelurahan tidak boleh kosong'
                //         }
                //     }
                // },
                nama_ktp: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Nama KTP tidak boleh kosong'
                        },
                        callback: {
                            message: 'Masukkan hanya huruf',
                            callback: function(value, validator, $field) {
                                if (!isValid(value)) {
                                    return {
                                        valid: false,
                                    };
                                } else {
                                    return {
                                        valid: true,
                                    };
                                }

                            }
                        }
                    }
                },
                jenis_kelamin: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Jenis Kelamin tidak boleh kosong'
                        }
                    }
                },
                tempat_lahir: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Tempat Lahir tidak boleh kosong'
                        },
                        callback: {
                            message: 'Masukkan hanya huruf',
                            callback: function(value, validator, $field) {
                                if (!isValid(value)) {
                                    return {
                                        valid: false,
                                    };
                                } else {
                                    return {
                                        valid: true,
                                    };
                                }

                            }
                        }
                    }
                },
                tgl_lahir: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Tgl Lahir tidak boleh kosong'
                        }
                    }
                },
                alamat_ktp: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Alamat KTP tidak boleh kosong'
                        }
                    }
                },
                nama_pemilik: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Nama Pemilik tidak boleh kosong'
                        },
                        callback: {
                            message: 'Masukkan hanya huruf',
                            callback: function(value, validator, $field) {
                                if (!isValid(value)) {
                                    return {
                                        valid: false,
                                    };
                                } else {
                                    return {
                                        valid: true,
                                    };
                                }

                            }
                        }
                    }
                },
                alamat_pemilik: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom Alamat Pemilik tidak boleh kosong'
                        }
                    }
                },
                // ktp: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Kolom KTP tidak boleh kosong'
                //         }
                //     }
                // },
                nik: {
                    validators: {
                        notEmpty: {
                            message: 'Kolom NIK tidak boleh kosong'
                        }
                    }
                }
            },
            err: {
                clazz: 'invalid-feedback'
            },
            control: {
                // The CSS class for valid control
                valid: 'is-valid',

                // The CSS class for invalid control
                invalid: 'is-invalid'
            },
            row: {
                invalid: 'has-danger'
            }
        });

        var grosirEngine = new Bloodhound({
            datumTokenizer: function(d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            cache: false,
            remote: {
                url: '{{ url('autocomplete/grosir') }}?q=%QUERY',
                wildcard: "%QUERY"
            }
        });

        $("#grosir").typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            source: grosirEngine.ttAdapter(),
            name: "grosir",
            displayKey: "grosir",
            templates: {
                suggestion: function(data) {
                    return Handlebars.compile([
                        "<div class=\"tt-dataset\">",
                        "<div>@{{ grosir }}</div>",
                        "</div>",
                    ].join(""))(data);
                },
                empty: [
                    "<div>grosir tidak ditemukan</div>"
                ]
            }
        }).bind("typeahead:selected", function(obj, datum, name) {
            $("#id_grosir").val(datum.id);
        }).bind("typeahead:change", function(obj, datum, name) {

        });

        var kelurahanEngine = new Bloodhound({
            datumTokenizer: function(d) {
                return d.tokens;
            },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            cache: false,
            remote: {
                url: '{{ url('autocomplete/kelurahan') }}?q=%QUERY',
                wildcard: "%QUERY"
            }
        });

        $("#kelurahan").typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            source: kelurahanEngine.ttAdapter(),
            name: "kelurahan",
            displayKey: "kelurahan",
            templates: {
                suggestion: function(data) {
                    return Handlebars.compile([
                        "<div class=\"tt-dataset\">",
                        "<div>@{{ kelurahan }}</div>",
                        "</div>",
                    ].join(""))(data);
                },
                empty: [
                    "<div>kelurahan tidak ditemukan</div>"
                ]
            }
        }).bind("typeahead:selected", function(obj, datum, name) {
            $("#id_kelurahan").val(datum.id);
        }).bind("typeahead:change", function(obj, datum, name) {

        });


    });
</script>
