<div class="modal-dialog modal-simple">

		{{ Form::model($presentaseKomisi,array('route' => array((!$presentaseKomisi->exists) ? 'presentase-komisi.store':'presentase-komisi.update',$presentaseKomisi->pk()),
	        'class'=>'modal-content','id'=>'presentase-komisi-form','method'=>(!$presentaseKomisi->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($presentaseKomisi->exists?'Edit':'Tambah').' Presentase Komisi' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('presentase_komisi','text')->model($presentaseKomisi)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#presentase-komisi-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	presentase_komisi : { validators: {
				        notEmpty: {
				          message: 'Kolom presentase_komisi tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
