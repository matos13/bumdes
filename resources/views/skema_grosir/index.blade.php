@extends('layouts.app')

@section('content')
<div class="page-header">
 <h1 class="page-title">Skema Grosir</h1>
 @include('layouts.inc.breadcrumb')
 <div class="page-header-actions">
   <a class="btn btn-block btn-primary data-modal" id="data-modal" href="#" onclick="show_modal('{{ route('skema-grosir.create') }}')" >Tambah</a>
 </div>
</div>
<div class="page-content">
 <!-- Panel Table Tools -->
 <div class="panel">
   <header class="panel-heading">
     <div class="form-group col-md-12">
       <div class="form-group">
         <!-- <h3 class="panel-title">Table Tools</h3> -->

       </div>
     </div>
   </header>
   <div class="panel-body">
         <!-- <table class="table table-bordered" id="users-table">

         </table> -->
         <table class="table table-hover dataTable table-striped w-full" id="skema-grosir-table">
           <thead>
             <tr>
               <th>No</th>
               <th>Barang</th>
               <th>Jumlah Minimal</th>
               <th>Jumlah Maksimal</th>
               <th>Harga Perbarang</th>
               <th>Status</th>
               {{--  <th>created_at</th>  --}}
               <th>Action</th>
             </tr>
           </thead>
         </table>
       </div>
     </div>
     <!-- End Panel Table Tools -->
   </div>
   <div class="modal fade" id="formModal" aria-hidden="true" aria-labelledby="formModalLabel" role="dialog" tabindex="-1">
   </div>

   @endsection

   @push('js')
   <script type="text/javascript">
    $(function() {
      $('.trash-ck').click(function(){
        if ($('.trash-ck').prop('checked')) {
          document.location = '{{ url("skema-grosir?status=trash") }}';
        } else {
          document.location = '{{ url("skema-grosir") }}';
        }
      });
      $('#skema-grosir-table').DataTable({
        stateSave: true,
        processing : true,
        serverSide : true,
        pageLength:20,
        ajax : {
          url:"{{ url('skema-grosir/load-data') }}",
          data: function (d) {

          }
        },
        columns: [
        { data: 'nomor', name: 'nomor',searchable:false,orderable:false },
        { data: 'barang', name: 'barang' },
        { data: 'jumlah_minimal', name: 'jumlah_minimal' },
        { data: 'jumlah_maksimal', name: 'jumlah_maksimal' },
        { data: 'harga_perbarang', name: 'harga_perbarang' },
        { data: 'status', name: 'Status', searchable:false,orderable:false , "render":function(data,type,row){
          if(data.flag_aktif =='Y')
          {
            return '<a class="btn btn-success btn-xs"  style="color:white; font-family:Arial" title="Nonaktifkan " onclick="con(\'' + data.id + '\',\'' + data.flag_aktif + '\') ">Aktif</a>';
          }
          else
          {
            return '<a class="btn btn-danger btn-xs"  style="color:white;font-family:Arial" title="Aktifkan" onclick="con(\'' + data.id + '\',\'' + data.flag_aktif + '\') ">Tidak Aktif</a>';
          }
        }						   
      },
                      // { data: 'created_at', name: 'created_at' },
                      { data: 'action', name: 'action', orderable: false, searchable: false },
                      ],
                      language: {
                        lengthMenu : '{{ "Menampilkan _MENU_ data" }}',
                        zeroRecords : '{{ "Data tidak ditemukan" }}' ,
                        info : '{{ "_PAGE_ dari _PAGES_ halaman" }}',
                        infoEmpty : '{{ "Data tidak ditemukan" }}',
                        infoFiltered : '{{ "(Penyaringan dari _MAX_ data)" }}',
                        loadingRecords : '{{ "Memuat data dari server" }}' ,
                        processing :    '{{ "Memuat data data" }}',
                        search :        '{{ "Pencarian:" }}',
                        paginate : {
                          first :     '{{ "<" }}' ,
                          last :      '{{ ">" }}' ,
                          next :      '{{ ">>" }}',
                          previous :  '{{ "<<" }}'
                        }
                      },
                      aoColumnDefs: [{
                        bSortable: false,
                        aTargets: [-1]
                      }],
                      iDisplayLength: 5,
                      aLengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        // sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
        // buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
      });
    });
    function con(id,status)
    {
      if(status=='Y')
      {
        swal({
          title: "Apakah anda yakin?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        }, function (isConfirm) {
          if (!isConfirm) return;
          window.location.href = "{{ url('skema-grosir/deactivate/')}}/"+id+"";
        });
      }
      else
      {
        swal({
          title: "Apakah anda yakin?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        }, function (isConfirm) {
          if (!isConfirm) return;
          window.location.href = "{{ url('skema-grosir/activate/')}}/"+id+"";
        });
      }
      
    }
  </script>
  @endpush
