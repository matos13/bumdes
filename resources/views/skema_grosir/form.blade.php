<div class="modal-dialog modal-simple">

	{{ Form::model($skemaGrosir,array('route' => array((!$skemaGrosir->exists) ? 'skema-grosir.store':'skema-grosir.update',$skemaGrosir->pk()),
	'class'=>'modal-content','id'=>'skema-grosir-form','method'=>(!$skemaGrosir->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($skemaGrosir->exists?'Edit':'Tambah').' Skema Grosir' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('id_barang','hidden')->model($skemaGrosir)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('barang',array('value'=>$skemaGrosir->exists?(isset($skemaGrosir->barang)?$skemaGrosir->barang->barang:null):null))->model(null)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('jumlah_minimal','text')->model($skemaGrosir)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('jumlah_maksimal','text')->model($skemaGrosir)->show() !!}
		{{-- {!! App\Console\Commands\Generator\Form::input('harga_perbarang','text')->model($skemaGrosir)->show() !!} --}}
		<div class="form-group row">
			<label for="harga_perbarang" class="col-form-label col-md-3">Harga Perbarang</label>
			<div class="col-md-7">
				<input name="harga_perbarang" id="harga_perbarang" class="form-control" type="text" value="{{isset($skemaGrosir->harga_perbarang)?rupiah($skemaGrosir->harga_perbarang):null}}" data-fv-field="harga_perbarang">
				<small class="invalid-feedback" data-fv-validator="notEmpty" data-fv-for="harga_perbarang" data-fv-result="NOT_VALIDATED" style="display: none;">Kolom harga_perbarang tidak boleh kosong</small>
			</div>
		</div>
		{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($skemaGrosir)->show() !!}  --}}
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($skemaGrosir->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#skema-grosir-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				harga_perbarang : { validators: {
					notEmpty: {
						message: 'Kolom harga_perbarang tidak boleh kosong'
					}
				}
			},flag_aktif : { validators: {
				notEmpty: {
					message: 'Kolom flag_aktif tidak boleh kosong'
				}
			}
		}
	},
	err: {
		clazz: 'invalid-feedback'
	},
	control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var barangEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/barang") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#barang").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: barangEngine.ttAdapter(),
			name: "barang",
			displayKey: "barang",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{barang}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>barang tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_barang").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			

		});
	var rupiah = document.getElementById('harga_perbarang');
	rupiah.addEventListener('keyup', function(e){
		rupiah.value = formatRupiah(this.value, 'Rp. ');
	});
</script>
