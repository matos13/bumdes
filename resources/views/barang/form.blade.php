<div class="modal-dialog modal-simple">

	{{ Form::model($barang,array('route' => array((!$barang->exists) ? 'barang.store':'barang.update',$barang->pk()),
	'class'=>'modal-content','id'=>'barang-form','method'=>(!$barang->exists) ? 'POST' : 'PUT','enctype'=>'multipart/form-data')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($barang->exists?'Edit':'Tambah').' Barang' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('barang','text')->model($barang)->show() !!}
		@if ($id_grosir)
		<input type="hidden" name="id_grosir" value="{{$id_grosir}}">	
		@else
		{!! App\Console\Commands\Generator\Form::input('id_grosir','hidden')->model($barang)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('grosir',array('value'=>$barang->exists?(isset($barang->grosir)?$barang->grosir->grosir:null):null))->model(null)->show() !!}
		@endif
		{!! App\Console\Commands\Generator\Form::input('id_sub_kategori_barang','hidden')->model($barang)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('sub_kategori_barang',array('value'=>$barang->exists?(isset($barang->sub_kategori_barang)?$barang->sub_kategori_barang->sub_kategori_barang:null):null))->model(null)->show() !!}
{{-- {!! App\Console\Commands\Generator\Form::input('harga_normal','text')->model($barang)->show() !!}
--}}
<div class="form-group row">
	<label for="harga_normal" class="col-form-label col-md-3">Harga Normal</label>
	<div class="col-md-7">
		<input name="harga_normal" id="harga_normal" class="form-control" type="text" value="{{isset($barang->harga_normal)?rupiah($barang->harga_normal):null}}" data-fv-field="harga_normal">
		<small class="invalid-feedback" data-fv-validator="notEmpty" data-fv-for="harga_normal" data-fv-result="NOT_VALIDATED" style="display: none;">Kolom harga tidak boleh kosong</small>
	</div>
</div>
	{!! App\Console\Commands\Generator\Form::input('stok','text')->model($barang)->show() !!}
	{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($barang)->show() !!}  --}}
	<div class="form-group row">
		<label for="kategori_barang" class="col-form-label col-md-3">Gambar</label>
		<div class="col-md-7">
			<input type="file" name="gambar" class="form-control" placeholder="" accept="image/jpeg">
		</div>
	</div>
	<div class="form-group row">
		<label for="provinsi" class="col-form-label col-md-3">Status</label>
		<div class="col-md-7">
			<label class="form-check-label">
				<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
			</label>
			<label class="form-check-label">
				<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($barang->flag_aktif=='N')
				checked
				@endif>Tidak Aktif
			</label>
		</div>
	</div>
	<div class="col-md-12 float-right">
		<div class="text-right">
			<button class="btn btn-primary" id="simpan">Simpan</button>
		</div>
	</div>
</div>

{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#barang-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				harga_normal : { validators: {
					notEmpty: {
						message: 'Kolom harga_normal tidak boleh kosong'
					}
				}
			},flag_aktif : { validators: {
				notEmpty: {
					message: 'Kolom flag_aktif tidak boleh kosong'
				}
			}
		}
	},
	err: {
		clazz: 'invalid-feedback'
	},
	control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var grosirEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/grosir") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#grosir").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: grosirEngine.ttAdapter(),
			name: "grosir",
			displayKey: "grosir",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{grosir}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>grosir tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_grosir").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			
			var sub_kategori_barangEngine = new Bloodhound({
				datumTokenizer: function(d) { return d.tokens; },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				cache: false,
				remote: {
					url: '{{ url("autocomplete/sub_kategori_barang") }}?q=%QUERY',
					wildcard: "%QUERY"
				}
			});

			$("#sub_kategori_barang").typeahead({
				hint: true,
				highlight: true,
				minLength: 1
			},
			{
				source: sub_kategori_barangEngine.ttAdapter(),
				name: "sub_kategori_barang",
				displayKey: "sub_kategori_barang",
				templates: {
					suggestion: function(data){
						return Handlebars.compile([
							"<div class=\"tt-dataset\">",
							"<div>@{{sub_kategori_barang}}</div>",
							"</div>",
							].join(""))(data);
						},
						empty: [
						"<div>sub_kategori_barang tidak ditemukan</div>"
						]
					}
				}).bind("typeahead:selected", function(obj, datum, name) {
					$("#id_sub_kategori_barang").val(datum.id);
				}).bind("typeahead:change", function(obj, datum, name) {

				});
				

			});
	var rupiah = document.getElementById('harga_normal');
	rupiah.addEventListener('keyup', function(e){
		rupiah.value = formatRupiah(this.value, 'Rp. ');
	});
</script>
