<div class="modal-dialog modal-simple">

	{{ Form::model($kategoriBarang,array('route' => array((!$kategoriBarang->exists) ? 'kategori-barang.store':'kategori-barang.update',$kategoriBarang->pk()),
	'class'=>'modal-content','id'=>'kategori-barang-form','method'=>(!$kategoriBarang->exists) ? 'POST' : 'PUT','enctype'=>'multipart/form-data')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($kategoriBarang->exists?'Edit':'Tambah').' Kategori Barang' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('kategori_barang','text')->model($kategoriBarang)->show() !!}
		{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($kategoriBarang)->show() !!}  --}}
		<div class="form-group row">
			<label for="kategori_barang" class="col-form-label col-md-3">Icon</label>
			<div class="col-md-7">
				<input type="file" name="icon" class="form-control" placeholder="" accept="image/png">
			</div>
		</div>
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($kategoriBarang->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#kategori-barang-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				flag_aktif : { validators: {
					notEmpty: {
						message: 'Kolom flag_aktif tidak boleh kosong'
					}
				}
			}
		},
		err: {
			clazz: 'invalid-feedback'
		},
		control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		

	});
</script>
