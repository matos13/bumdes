

<html>
<head>
    <title>Invoice</title>
    <style>

        #tabel
        {
            font-size:15px;
            border-collapse:collapse;
        }
        #tabel  td
        {
            padding-left:5px;
            border: 1px solid black;
        }
    </style>
</head>
<body style='font-family:tahoma; font-size:8pt;'>
    <center><table style='width:550px; font-size:8pt; font-family:calibri; border-collapse: collapse;' border = '0'>
        <td width='60%' align='left' style='padding-right:80px; vertical-align:top'>
            <img width="80px"src="{{ asset('public') }}/logo.png">
            {{-- <span style='font-size:12pt'><b>Bumdes</b></span> --}}
        {{-- </br>Alamat Toko Alamat Toko Alamat Toko Alamat Toko Alamat Toko Alamat Toko Alamat Toko Alamat Toko Alamat Toko Alamat Toko </br>Telp : 0594094545 --}}
    </td>
    <td style='vertical-align:top' width='40%' align='left'>
        <b><span style='font-size:12pt'>INVOICE</span></b></br>
        No Trans &nbsp; :  {{$data[0]->nomor_order}}</br>
        Tanggal &nbsp;&nbsp;&nbsp;&nbsp;: {{tgl_indo($data[0]->tanggal_order)}}</br>
    </td>
</table>
<table style='width:550px; font-size:8pt; font-family:calibri; border-collapse: collapse;' border = '0'>
    <td width='70%' align='left' style='padding-right:80px; vertical-align:top'>
        Nama   &nbsp;&nbsp;&nbsp;&nbsp; : {{$data[0]->warung}}</br>
        Alamat &nbsp;&nbsp; : -
    </td>
    <td style='vertical-align:top' width='30%' align='left'>
        No Telp : -
    </td>
</table>
<table cellspacing='0' style='width:550px; font-size:8pt; font-family:calibri;  border-collapse: collapse;' border='1'>

    <tr align='center'>
        <td width='4%'>No</td>
        <td width='20%'>Nama Barang</td>
        <td width='13%'>Harga</td>
        <td width='4%'>Qty</td>
        <td width='13%'>Total Harga</td>
        <tr>
            <?php
            $total=0;
            $no=1;
            ?>
            @foreach ($data as $item)
            <?php
            $total=$total+$item->subtotal;
            ?>
            <tr>
                <td align='center'>{{$no++}}</td>
                <td>{{$item->barang}}</td>
                <td align="right">{{rupiahTanpaKoma($item->harga)}}</td>
                <td align="center">{{$item->qty}}</td>
                <td align="right">{{rupiahTanpaKoma($item->subtotal)}}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan = '4'>
                    <div style='text-align:right'>Total Yang Harus Di Bayar Adalah : </div>
                </td>
                <td style='text-align:right'>{{rupiahTanpaKoma($total)}}</td>
            </tr>
            <tr>
                <td colspan = '5'>
                    <div style='text-align:right'>Terbilang : <i>{{terbilang($total)}}</i></div>
                </td>
            </tr>
{{--             <tr>
                <td colspan = '4'>
                    <div style='text-align:right'>Cash : </div>
                </td>
                <td style='text-align:right'>Rp2.460.000,00</td>
            </tr>
            <tr>
                <td colspan = '4'>
                    <div style='text-align:right'>Kembalian : </div>
                </td><td style='text-align:right'>Rp0,00</td>
            </tr>
            <tr>
                <td colspan = '4'>
                    <div style='text-align:right'>DP : </div>
                </td>
                <td style='text-align:right'>Rp0,00</td>
            </tr>
            <tr>
                <td colspan = '4'>
                    <div style='text-align:right'>Sisa : </div>
                </td>
                <td style='text-align:right'>Rp0,00</td>
            </tr> --}}
        </table>
        <br>
        <table style='width:650; font-size:7pt;' cellspacing='2'>
            <tr>
                <td align='center'>Diterima Oleh,</br></br></br></br><u>(.................)</u></td>
                <td style='border:0px solid black; padding:5px; text-align:left; width:30%'></td>
                <td align='center'>TTD,</br></br></br></br><u>(................)</u></td>
            </tr>
        </table>
    </center>
    <center><button onclick="cetak()" id="cetak">Cetak</button></center>
</body>
</html>
<script type="text/javascript">
    function cetak(){
        document.getElementById("cetak").style.display = 'none';
        window.print();
    }
</script>