@extends('layouts.app')

@section('content')
<div class="page-header">
 <h1 class="page-title">Detail Order</h1>
 @include('layouts.inc.breadcrumb')
 {{--  <div class="page-header-actions">
   <a class="btn btn-block btn-primary data-modal" id="data-modal" href="#" onclick="show_modal('{{ route('order-datum.create') }}')" >Tambah</a>
 </div>  --}}
</div>
<div class="page-content">
 <!-- Panel Table Tools -->
 <div class="panel">
   <header class="panel-heading">
     <div class="form-group col-md-12">
       <div class="form-group">
         <!-- <h3 class="panel-title">Table Tools</h3> -->
        
       </div>
     </div>
   </header>
   <div class="panel-body">
        <table>
            <tr>
                <td>NO ORDER</td>
                <td style="width: 20px" align="center">:</td>
                <td> {{$data[0]->nomor_order}}</td>
            </tr>
            <tr>
                <td>WARUNG</td>
                <td style="width: 20px" align="center">:</td>
                <td> {{$data[0]->warung}}</td>
            </tr>
            <tr>
                <td>NAMA PEMILIK</td>
                <td style="width: 20px" align="center">:</td>
                <td> {{$data[0]->nama_pemilik}}</td>
            </tr>
            <tr>
                <td>TANGGAL ORDER</td>
                <td style="width: 20px" align="center">:</td>
                <td> {{$data[0]->tanggal_order}}</td>
            </tr>
        </table>
        <fieldset>
            <legend><hr></legend>
            <table class="table table-hover dataTable table-striped table-bordered w-full" >
                <thead align="center">
                    <tr>
                        <th>No</th>
                        <th>Barang</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $total=0;
                    $no=1;
                ?>
                    @foreach ($data as $item)
                    <?php
                        $total=$total+$item->subtotal;
                    ?>
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$item->barang}}</td>
                            <td>{{$item->qty}}</td>
                            <td align="right">{{rupiahTanpaKoma($item->harga)}}</td>
                            <td align="right">{{rupiahTanpaKoma($item->subtotal)}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" align="right">Total</td>
                        <td align="right">{{rupiahTanpaKoma($total)}}</td>
                    </tr>
                </tfoot>
            </table>
        </fieldset>
        <hr>
        <div style="float: right">
            <button onclick="back()"class="btn btn-sm btn-success"><i class="icon md-mail-reply"></i>Kembali</button>
            <button onclick="batal({{$data[0]->id}})"class="btn btn-sm btn-danger"><i class="icon md-close"></i>Batal</button>
            <button onclick="verif({{$data[0]->id}})" class="btn btn-sm btn-info"><i class="icon md-check"></i>Verif</button>
        </div>
       </div>
     </div>
   </div>
   @endsection

   @push('js')
   <script>
    function verif(id)
    {
        swal({
            title: "Apakah anda yakin?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: false
        }, function (isConfirm) {
            if (!isConfirm) return;
            window.location.href = "{{ url('order-datum/confirm/')}}/"+id+"";
        });
    
    }
    function back(params) {
        window.location.href = "{{ url('order-datum')}}";
    }
    function batal(params) {
        
    }
   </script>
  @endpush
