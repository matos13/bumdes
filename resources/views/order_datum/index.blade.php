@extends('layouts.app')

@section('content')
<div class="page-header">
 <h1 class="page-title">Verifikasi Order</h1>
 @include('layouts.inc.breadcrumb')
 {{--  <div class="page-header-actions">
   <a class="btn btn-block btn-primary data-modal" id="data-modal" href="#" onclick="show_modal('{{ route('order-datum.create') }}')" >Tambah</a>
 </div>  --}}
</div>
<div class="page-content">
 <!-- Panel Table Tools -->
 <div class="panel">
   <header class="panel-heading">
     <div class="form-group col-md-12">
       <div class="form-group">
         <!-- <h3 class="panel-title">Table Tools</h3> -->

       </div>
     </div>
   </header>
   <div class="panel-body">
         <!-- <table class="table table-bordered" id="users-table">

         </table> -->
         <table class="table table-hover dataTable table-striped w-full" id="order-datum-table">
           <thead>
             <tr>
               <th>No</th>
               <th>No Order</th>
               <th>Warung</th>
               <th>Nama</th>
               <th>Tanggal Order</th>
               <th>Total Order</th>
               <th>Status</th>
               <th>Action</th>
             </tr>
           </thead>
         </table>
       </div>
     </div>
     <!-- End Panel Table Tools -->
   </div>
   <div class="modal fade" id="formModal" aria-hidden="true" aria-labelledby="formModalLabel" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Pilih Driver</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="example-wrap" data-select2-id="117">
            <h4 class="example-title">Driver</h4>
            <div class="example" data-select2-id="116">
              <select class="form-control select2-hidden-accessible" data-plugin="select2" data-placeholder="Select a State" data-allow-clear="true" data-select2-id="6" tabindex="-1" aria-hidden="true">
                <option data-select2-id="8"></option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>
   @endsection

   @push('js')
   <script type="text/javascript">
   function detail(id){
    window.location.href = 'order-datum/detail/'+id;
   }
   function cetak(id){
    window.open('order-datum/cetak/'+id);
   }
   function driver(id){
    $('#formModal').modal('show');
   }
    $(function() {
      $('.trash-ck').click(function(){
        if ($('.trash-ck').prop('checked')) {
          document.location = '{{ url("order-datum?status=trash") }}';
        } else {
          document.location = '{{ url("order-datum") }}';
        }
      });
      $('#order-datum-table').DataTable({
        stateSave: true,
        processing : true,
        serverSide : true,
        pageLength:20,
        ajax : {
          url:"{{ url('order-datum/load-data') }}",
          data: function (d) {

          }
        },
        columns: [
        { data: 'nomor', name: 'nomor',searchable:false,orderable:false },
        { data: 'nomor_order', name: 'nomor_order' },
        { data: 'warung', name: 'warung' },
        { data: 'nama_pemilik', name: 'nama_pemilik' },
        { data: 'tanggal_order', name: 'tanggal_order' },
        { data: 'total', name: 'total' },
        { data: 'status_order', name: 'status_order' },
        { data: 'aksi', name: 'aksi', searchable:false,orderable:false , "render":function(data,type,row){
          // console.log(data);
            if(data.status_order=='dipesan')
                  {
                    return '<button onclick="detail(\''+data.id+'\')"" class="btn  btn-info btn-sm waves-effect waves-classic" > <i class="icon md-menu"></i>Detail</button>';
                  }
                  else
                  {
                    return '<button style="margin-bottom: 2px;" onclick="cetak(\''+data.id+'\')"" class="btn  btn-info btn-sm waves-effect waves-classic" > <i class="icon md-print"></i>Invoice</button><br><button onclick="driver(\''+data.id+'\')"" class="btn  btn-warning btn-sm waves-effect waves-classic" > <i class="icon  md-plus"></i>Driver</button>';
                  }
                }						   
            },
        // { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        language: {
          lengthMenu : '{{ "Menampilkan _MENU_ data" }}',
          zeroRecords : '{{ "Data tidak ditemukan" }}' ,
          info : '{{ "_PAGE_ dari _PAGES_ halaman" }}',
          infoEmpty : '{{ "Data tidak ditemukan" }}',
          infoFiltered : '{{ "(Penyaringan dari _MAX_ data)" }}',
          loadingRecords : '{{ "Memuat data dari server" }}' ,
          processing :    '{{ "Memuat data data" }}',
          search :        '{{ "Pencarian:" }}',
          paginate : {
            first :     '{{ "<" }}' ,
            last :      '{{ ">" }}' ,
            next :      '{{ ">>" }}',
            previous :  '{{ "<<" }}'
          }
        },
        aoColumnDefs: [{
          bSortable: false,
          aTargets: [-1]
        }],
        iDisplayLength: 5,
        aLengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        // sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
        // buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
      });
    });
  </script>
  @endpush
