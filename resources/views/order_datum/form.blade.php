<div class="modal-dialog modal-simple">

	{{ Form::model($orderDatum,array('route' => array((!$orderDatum->exists) ? 'order-datum.store':'order-datum.update',$orderDatum->pk()),
	'class'=>'modal-content','id'=>'order-datum-form','method'=>(!$orderDatum->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($orderDatum->exists?'Edit':'Tambah').' Order Data' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('id_user_apk','hidden')->model($orderDatum)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('user_apk',array('value'=>$orderDatum->exists?(isset($orderDatum->user_apk)?$orderDatum->user_apk->user_apk:null):null))->model(null)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('status_order','text')->model($orderDatum)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('tanggal_order','text')->model($orderDatum)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('latitude','text')->model($orderDatum)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('longitude','text')->model($orderDatum)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('total','text')->model($orderDatum)->show() !!}
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#order-datum-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				
			},
			err: {
				clazz: 'invalid-feedback'
			},
			control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var user_apkEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/user_apk") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#user_apk").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: user_apkEngine.ttAdapter(),
			name: "user_apk",
			displayKey: "user_apk",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{user_apk}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>user_apk tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_user_apk").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			

		});
	</script>
