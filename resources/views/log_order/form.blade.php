<div class="modal-dialog modal-simple">

		{{ Form::model($logOrder,array('route' => array((!$logOrder->exists) ? 'log-order.store':'log-order.update',$logOrder->pk()),
	        'class'=>'modal-content','id'=>'log-order-form','method'=>(!$logOrder->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($logOrder->exists?'Edit':'Tambah').' Log Order' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('id_order','hidden')->model($logOrder)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('order',array('value'=>$logOrder->exists?(isset($logOrder->order)?$logOrder->order->order:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('status','text')->model($logOrder)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'keterangan' )->model($logOrder)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#log-order-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var orderEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/order") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#order").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: orderEngine.ttAdapter(),
									name: "order",
									displayKey: "order",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{order}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>order tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_order").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
