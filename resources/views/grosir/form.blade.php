<div class="modal-dialog modal-simple">

	{{ Form::model($grosir,array('route' => array((!$grosir->exists) ? 'grosir.store':'grosir.update',$grosir->pk()),
	'class'=>'modal-content','id'=>'grosir-form','method'=>(!$grosir->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($grosir->exists?'Edit':'Tambah').' Grosir' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('grosir','text')->model($grosir)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('id_kelurahan','hidden')->model($grosir)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('kelurahan',array('value'=>$grosir->exists?(isset($grosir->kelurahan)?$grosir->kelurahan->kelurahan:null):null))->model(null)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('alamat','text')->model($grosir)->show() !!}
		{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($grosir)->show() !!}  --}}
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($grosir->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#grosir-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				flag_aktif : { validators: {
					notEmpty: {
						message: 'Kolom flag_aktif tidak boleh kosong'
					}
				}
			}
		},
		err: {
			clazz: 'invalid-feedback'
		},
		control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var kelurahanEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/kelurahan") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#kelurahan").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: kelurahanEngine.ttAdapter(),
			name: "kelurahan",
			displayKey: "kelurahan",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{kelurahan}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>kelurahan tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_kelurahan").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			

		});
	</script>

