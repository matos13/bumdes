<div class="modal-dialog modal-simple">

		{{ Form::model($bumde,array('route' => array((!$bumde->exists) ? 'bumde.store':'bumde.update',$bumde->pk()),
	        'class'=>'modal-content','id'=>'bumde-form','method'=>(!$bumde->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($bumde->exists?'Edit':'Tambah').' Bumdes' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('bumdes','text')->model($bumde)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('nama_koordinator','text')->model($bumde)->show() !!}
																        {!! App\Console\Commands\Generator\Form::textarea( 'alamat' )->model($bumde)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_kelurahan','hidden')->model($bumde)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kelurahan',array('value'=>$bumde->exists?(isset($bumde->kelurahan)?$bumde->kelurahan->kelurahan:null):null))->model(null)->show() !!}
	<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="status_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="status_aktif" value='N' @if ($bumde->status_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#bumde-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	bumdes : { validators: {
				        notEmpty: {
				          message: 'Kolom bumdes tidak boleh kosong'
							}
						}
					},nama_koordinator : { validators: {
				        notEmpty: {
				          message: 'Kolom nama_koordinator tidak boleh kosong'
							}
						}
					},status_aktif : { validators: {
				        notEmpty: {
				          message: 'Kolom status_aktif tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var kelurahanEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kelurahan") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kelurahan").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kelurahanEngine.ttAdapter(),
									name: "kelurahan",
									displayKey: "kelurahan",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kelurahan}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kelurahan tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kelurahan").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
