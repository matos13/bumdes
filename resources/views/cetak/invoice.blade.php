<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
</head>
<style>
    .table {
        width: 100%;
        border-collapse: collapse;
    }

</style>

<body>
    <center>
        <h1>Invoice</h1>
    </center>
    <table>
        <tr>
            <td>Tgl Order</td>
            <td style="width: 30px;text-align: center">:</td>
            <td>{{ $data[0]->tanggal_order }}</td>
        </tr>
        <tr>
            <td>Grosir</td>
            <td style="width: 30px;text-align: center">:</td>
            <td>{{ $data[0]->grosir }}</td>
        </tr>
    </table>
    <hr>
    <center><b><i>Detail Barang :</i></b></center>
    <br>
    <table border="1" class="table table-bordered table-striped w-full" id="pembayaran-table">
        <thead style="text-align: center">
            <tr>
                <th>No</th>
                <th>Barang</th>
                <th>Qty</th>
                <th>Harga Satuan</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = 0; ?>
            @foreach ($data as $n => $item)
                <tr>
                    <td style="text-align: center">{{ $n + 1 }}</td>
                    <td>{{ $item->barang }}</td>
                    <td style="text-align: center">{{ $item->qty }}</td>
                    <td style="text-align: right">{{ rupiahTanpaKoma($item->harga) }}</td>
                    <td style="text-align: right">{{ rupiahTanpaKoma($item->subtotal) }}</td>
                </tr>
                <?php $total = $total + $item->subtotal; ?>
            @endforeach
            <tr>
                <td style="text-align: right" colspan="4"><b><i>Total</i></b></td>
                <td style="text-align: right">{{ rupiahTanpaKoma($total) }}</td>
            </tr>
            <tr>
                <td style="text-align: right" colspan="4"><b><i>Bayar</i></b></td>
                <td style="text-align: right">{{ rupiahTanpaKoma($total) }}</td>
            </tr>
        </tbody>
    </table>
    <br>
    <table style="width: 100%;text-align: center">
        <tr>
            <td>Hormat Kami</td>
        </tr>
        <tr>
            <td><br><br></td>
        </tr>
        <tr>
            <td>(.........................)</td>
        </tr>
    </table>
</body>

</html>
