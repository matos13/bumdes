<div class="modal-dialog modal-simple">

		{{ Form::model($komisi,array('route' => array((!$komisi->exists) ? 'komisi.store':'komisi.update',$komisi->pk()),
	        'class'=>'modal-content','id'=>'komisi-form','method'=>(!$komisi->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($komisi->exists?'Edit':'Tambah').' Komisi' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('id_bumdes','hidden')->model($komisi)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('bumdes',array('value'=>$komisi->exists?(isset($komisi->bumdes)?$komisi->bumdes->bumdes:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('total_belanja','text')->model($komisi)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('presentase_komisi','text')->model($komisi)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('total_komisi','text')->model($komisi)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('tanggal_komisi','text')->model($komisi)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('status_bayar','text')->model($komisi)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#komisi-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	total_belanja : { validators: {
				        notEmpty: {
				          message: 'Kolom total_belanja tidak boleh kosong'
							}
						}
					},presentase_komisi : { validators: {
				        notEmpty: {
				          message: 'Kolom presentase_komisi tidak boleh kosong'
							}
						}
					},total_komisi : { validators: {
				        notEmpty: {
				          message: 'Kolom total_komisi tidak boleh kosong'
							}
						}
					},status_bayar : { validators: {
				        notEmpty: {
				          message: 'Kolom status_bayar tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var bumdesEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/bumdes") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#bumdes").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: bumdesEngine.ttAdapter(),
									name: "bumdes",
									displayKey: "bumdes",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{bumdes}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>bumdes tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_bumdes").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
