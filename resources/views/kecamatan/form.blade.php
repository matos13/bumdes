<div class="modal-dialog modal-simple">

	{{ Form::model($kecamatan,array('route' => array((!$kecamatan->exists) ? 'kecamatan.store':'kecamatan.update',$kecamatan->pk()),
	'class'=>'modal-content','id'=>'kecamatan-form','method'=>(!$kecamatan->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($kecamatan->exists?'Edit':'Tambah').' Kecamatan' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('kode','text')->model($kecamatan)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('kecamatan','text')->model($kecamatan)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('id_kabupaten','hidden')->model($kecamatan)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('kabupaten',array('value'=>$kecamatan->exists?(isset($kecamatan->kabupaten)?$kecamatan->kabupaten->kabupaten:null):null))->model(null)->show() !!}
		{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($kecamatan)->show() !!}  --}}
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($kecamatan->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#kecamatan-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				flag_aktif : { validators: {
					notEmpty: {
						message: 'Kolom flag_aktif tidak boleh kosong'
					}
				}
			}
		},
		err: {
			clazz: 'invalid-feedback'
		},
		control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var kabupatenEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/kabupaten") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#kabupaten").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: kabupatenEngine.ttAdapter(),
			name: "kabupaten",
			displayKey: "kabupaten",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{kabupaten}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>kabupaten tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_kabupaten").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			

		});
	</script>
