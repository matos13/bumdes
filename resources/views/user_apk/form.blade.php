<div class="modal-dialog modal-simple">

		{{ Form::model($userApk,array('route' => array((!$userApk->exists) ? 'user-apk.store':'user-apk.update',$userApk->pk()),
	        'class'=>'modal-content','id'=>'user-apk-form','method'=>(!$userApk->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($userApk->exists?'Edit':'Tambah').' User Apk' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('username','text')->model($userApk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('nohp','text')->model($userApk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_kelurahan','hidden')->model($userApk)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('kelurahan',array('value'=>$userApk->exists?(isset($userApk->kelurahan)?$userApk->kelurahan->kelurahan:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('status','text')->model($userApk)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('password','text')->model($userApk)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#user-apk-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	status : { validators: {
				        notEmpty: {
				          message: 'Kolom status tidak boleh kosong'
							}
						}
					},password : { validators: {
				        notEmpty: {
				          message: 'Kolom password tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var kelurahanEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/kelurahan") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#kelurahan").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: kelurahanEngine.ttAdapter(),
									name: "kelurahan",
									displayKey: "kelurahan",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{kelurahan}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>kelurahan tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_kelurahan").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
