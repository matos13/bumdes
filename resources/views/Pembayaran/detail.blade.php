@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1 class="page-title">Detail Pembayaran</h1>
        @include('layouts.inc.breadcrumb')
    </div>
    <div class="page-content">
        <div class="panel">
            <header class="panel-heading">
                <div class="form-group col-md-12">
                    <div class="form-group">
                    </div>
                </div>
            </header>
            <div class="panel-body">
                <table>
                    <tr>
                        <td>Tgl Order</td>
                        <td style="width: 30px;text-align: center">:</td>
                        <td>{{ $data[0]->tanggal_order }}</td>
                    </tr>
                    <tr>
                        <td>Grosir</td>
                        <td style="width: 30px;text-align: center">:</td>
                        <td>{{ $data[0]->grosir }}</td>
                    </tr>
                </table>
                <hr>
                <center><b><i>Detail Barang :</i></b></center>
                <br>
                <table class="table table-bordered table-striped w-full" id="pembayaran-table">
                    <thead style="text-align: center">
                        <tr>
                            <th>No</th>
                            <th>Barang</th>
                            <th>Qty</th>
                            <th>Harga Satuan</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total = 0; ?>
                        @foreach ($data as $n => $item)
                            <tr>
                                <td style="text-align: center">{{ $n + 1 }}</td>
                                <td>{{ $item->barang }}</td>
                                <td style="text-align: center">{{ $item->qty }}</td>
                                <td style="text-align: right">{{ rupiahTanpaKoma($item->harga) }}</td>
                                <td style="text-align: right">{{ rupiahTanpaKoma($item->subtotal) }}</td>
                            </tr>
                            <?php $total = $total + $item->subtotal; ?>
                        @endforeach
                        <tr>
                            <td colspan="4"><b><i>Total</i></b></td>
                            <td style="text-align: right">{{ rupiahTanpaKoma($total) }}</td>
                        </tr>
                    </tbody>
                </table>
                <div style="text-align: right">
                    <button class="btn btn-warning" onclick="kembali()">Kembali</button>
                    <button class="btn btn-success" onclick="bayar({{ $data[0]->id }})">Bayar</button>
                </div>
            </div>
        </div>
        <!-- End Panel Table Tools -->
    </div>
    <div class="modal fade" id="formModal" aria-hidden="true" aria-labelledby="formModalLabel" role="dialog" tabindex="-1">
    </div>

@endsection

@push('js')
    <script>
        function kembali() {
            var APP_URL = {!! json_encode(url('/')) !!};
            window.location.href = APP_URL + '/pembayaran';
        }

        function bayar(id) {
            var APP_URL = {!! json_encode(url('/')) !!};
            swal({
                title: "Apakah anda yakin?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            }, function(isConfirm) {
                if (!isConfirm) return;
                window.location.href = APP_URL + '/pembayaran/simpan/' + id;
            });
        }
    </script>
@endpush
