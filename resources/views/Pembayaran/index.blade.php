@extends('layouts.app')

@section('content')
    <div class="page-header">
        <h1 class="page-title">Pembayaran</h1>
        @include('layouts.inc.breadcrumb')
    </div>
    <div class="page-content">
        <div class="panel">
            <header class="panel-heading">
                <div class="form-group col-md-12">
                    <div class="form-group">
                    </div>
                </div>
            </header>
            <div class="panel-body">
                <table class="table table-hover dataTable table-striped w-full" id="pembayaran-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Order</th>
                            <th>Grosir</th>
                            <th>Total</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <!-- End Panel Table Tools -->
    </div>
    <div class="modal fade" id="formModal" aria-hidden="true" aria-labelledby="formModalLabel" role="dialog" tabindex="-1">
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        function detail(id) {
            var APP_URL = {!! json_encode(url('/')) !!};
            window.location.href = APP_URL + '/pembayaran/detail/' + id;
        }
        $(function() {
            $('.trash-ck').click(function() {
                if ($('.trash-ck').prop('checked')) {
                    document.location = '{{ url('provinsi?status=trash') }}';
                } else {
                    document.location = '{{ url('provinsi') }}';
                }
            });
            $('#pembayaran-table').DataTable({
                stateSave: true,
                processing: true,
                serverSide: true,
                pageLength: 20,
                ajax: {
                    url: "{{ url('pembayaran/load-data') }}",
                    data: function(d) {

                    }
                },
                columns: [{
                        data: 'nomor',
                        name: 'nomor',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'tanggal_order',
                        name: 'tanggal_order'
                    },
                    {
                        data: 'grosir',
                        name: 'grosir'
                    },
                    {
                        data: 'total',
                        name: 'total',
                        orderable: false,
                        searchable: false
                    },
                    // { data: 'created_at', name: 'created_at' },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                language: {
                    lengthMenu: '{{ 'Menampilkan _MENU_ data' }}',
                    zeroRecords: '{{ 'Data tidak ditemukan' }}',
                    info: '{{ '_PAGE_ dari _PAGES_ halaman' }}',
                    infoEmpty: '{{ 'Data tidak ditemukan' }}',
                    infoFiltered: '{{ '(Penyaringan dari _MAX_ data)' }}',
                    loadingRecords: '{{ 'Memuat data dari server' }}',
                    processing: '{{ 'Memuat data data' }}',
                    search: '{{ 'Pencarian:' }}',
                    paginate: {
                        first: '{{ '<' }}',
                        last: '{{ '>' }}',
                        next: '{{ '>>' }}',
                        previous: '{{ '<<' }}'
                    }
                },
                aoColumnDefs: [{
                    bSortable: false,
                    aTargets: [-1]
                }],
                iDisplayLength: 5,
                aLengthMenu: [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                // sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
                // buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
            });
        });
    </script>
@endpush
