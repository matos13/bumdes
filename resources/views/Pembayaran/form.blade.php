<div class="modal-dialog modal-simple">

	{{ Form::model($provinsi,array('route' => array((!$provinsi->exists) ? 'provinsi.store':'provinsi.update',$provinsi->pk()),
	'class'=>'modal-content','id'=>'provinsi-form','method'=>(!$provinsi->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($provinsi->exists?'Edit':'Tambah').' Provinsi' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('kode','text')->model($provinsi)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('provinsi','text')->model($provinsi)->show() !!}
		{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','radio')->model($provinsi)->show() !!}  --}}
		{{--  {{$provinsi}}  --}}
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($provinsi->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="text-right">
			<button class="btn btn-primary" id="simpan">Simpan</button>
		</div>
	</div>
</div>

{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#provinsi-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				flag_aktif : { validators: {
					notEmpty: {
						message: 'Kolom flag_aktif tidak boleh kosong'
					}
				}
			}
		},
		err: {
			clazz: 'invalid-feedback'
		},
		control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		

	});
</script>
