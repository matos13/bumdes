<div class="modal-dialog modal-simple">

		{{ Form::model($detailPaketPromo,array('route' => array((!$detailPaketPromo->exists) ? 'detail-paket-promo.store':'detail-paket-promo.update',$detailPaketPromo->pk()),
	        'class'=>'modal-content','id'=>'detail-paket-promo-form','method'=>(!$detailPaketPromo->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($detailPaketPromo->exists?'Edit':'Tambah').' Detail Paket Promo' }}</h4>
    </div>
    <div class="modal-body">
	 {!! App\Console\Commands\Generator\Form::input('id_barang','hidden')->model($detailPaketPromo)->showHidden() !!}
	{!! App\Console\Commands\Generator\Form::autocomplete('barang',array('value'=>$detailPaketPromo->exists?(isset($detailPaketPromo->barang)?$detailPaketPromo->barang->barang:null):null))->model(null)->show() !!}
	<div class="form-group row">
		<label for="provinsi" class="col-form-label col-md-3">Tanggal Awal</label>
		<div class="col-md-7">
			<div class="input-group date" data-provide="datepicker">
				<input type="text" class="form-control" name="tanggal_awal" value="{{isset($detailPaketPromo->tanggal_awal)?tanggalinput($detailPaketPromo->tanggal_awal):null}}">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label for="provinsi" class="col-form-label col-md-3">Tanggal Akhir</label>
		<div class="col-md-7">
			<div class="input-group date" data-provide="datepicker">
				<input type="text" class="form-control" name="tanggal_akhir" value="{{isset($detailPaketPromo->tanggal_akhir)?tanggalinput($detailPaketPromo->tanggal_akhir):null}}">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>
	<!-- {!! App\Console\Commands\Generator\Form::input('tanggal_awal','text')->model($detailPaketPromo)->show() !!}
	 {!! App\Console\Commands\Generator\Form::input('tanggal_akhir','text')->model($detailPaketPromo)->show() !!} -->
	 <!-- {!! App\Console\Commands\Generator\Form::input('status_aktif','text')->model($detailPaketPromo)->show() !!} -->
	 <!-- {!! App\Console\Commands\Generator\Form::input('id_paket_promo','hidden')->model($detailPaketPromo)->showHidden() !!}
	{!! App\Console\Commands\Generator\Form::autocomplete('paket_promo',array('value'=>$detailPaketPromo->exists?(isset($detailPaketPromo->paket_promo)?$detailPaketPromo->paket_promo->paket_promo:null):null))->model(null)->show() !!} -->
	<div class="form-group row">
		<label for="provinsi" class="col-form-label col-md-3">Status</label>
		<div class="col-md-7">
			<label class="form-check-label">
				<input type="radio" class="form-check-input" name="status_aktif" value='Y' checked>Aktif
			</label>
			<label class="form-check-label">
				<input type="radio" class="form-check-input" name="status_aktif" value='N' @if ($detailPaketPromo->status_aktif=='N')
				checked
				@endif>Tidak Aktif
			</label>
		</div>
	</div>
	<input type="hidden" name="id_paket_promo" value="{{$id_paket_promo}}">
		<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#detail-paket-promo-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	status_aktif : { validators: {
				        notEmpty: {
				          message: 'Kolom status_aktif tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var paket_promoEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/paket_promo") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#paket_promo").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: paket_promoEngine.ttAdapter(),
									name: "paket_promo",
									displayKey: "paket_promo",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{paket_promo}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>paket_promo tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_paket_promo").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var barangEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/barang") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#barang").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: barangEngine.ttAdapter(),
									name: "barang",
									displayKey: "barang",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{barang}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>barang tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_barang").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
