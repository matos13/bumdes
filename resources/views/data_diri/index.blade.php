@extends('layouts.app')

@section('content')
<div class="page-header">
 <h1 class="page-title">Data Diri</h1>
 @include('layouts.inc.breadcrumb')
 <div class="page-header-actions">
   <a class="btn btn-block btn-primary data-modal" id="data-modal" href="#" onclick="show_modal('{{ route('data-diri.create') }}')" >Tambah</a>
 </div>
</div>
<div class="page-content">
 <!-- Panel Table Tools -->
 <div class="panel">
   <header class="panel-heading">
     <div class="form-group col-md-12">
       <div class="form-group">
         <!-- <h3 class="panel-title">Table Tools</h3> -->

       </div>
     </div>
   </header>
   <div class="panel-body">
         <!-- <table class="table table-bordered" id="users-table">

         </table> -->
         <table class="table table-hover dataTable table-striped w-full" id="data-diri-table">
           <thead>
             <tr>
               <th>No</th>
               <th>id_user_apk</th>
               <th>nik</th>
               <th>jenis_kelamin</th>
               <th>tempat_lahir</th>
               <th>tanggal_lahir</th>
               <th>alamat_pemilik</th>
               <th>nama_pemilik</th>
               <th>foto_ktp</th>
               <th>id_warung</th>
               <th>created_at</th>
               <th>Action</th>
             </tr>
           </thead>
         </table>
       </div>
     </div>
     <!-- End Panel Table Tools -->
   </div>
   <div class="modal fade" id="formModal" aria-hidden="true" aria-labelledby="formModalLabel" role="dialog" tabindex="-1">
   </div>

   @endsection

   @push('js')
   <script type="text/javascript">
    $(function() {
      $('.trash-ck').click(function(){
        if ($('.trash-ck').prop('checked')) {
          document.location = '{{ url("data-diri?status=trash") }}';
        } else {
          document.location = '{{ url("data-diri") }}';
        }
      });
      $('#data-diri-table').DataTable({
        stateSave: true,
        processing : true,
        serverSide : true,
        pageLength:20,
        ajax : {
          url:"{{ url('data-diri/load-data') }}",
          data: function (d) {

          }
        },
        columns: [
        { data: 'nomor', name: 'nomor',searchable:false,orderable:false },
        { data: 'id_user_apk', name: 'id_user_apk' },
        { data: 'nik', name: 'nik' },
        { data: 'jenis_kelamin', name: 'jenis_kelamin' },
        { data: 'tempat_lahir', name: 'tempat_lahir' },
        { data: 'tanggal_lahir', name: 'tanggal_lahir' },
        { data: 'alamat_pemilik', name: 'alamat_pemilik' },
        { data: 'nama_pemilik', name: 'nama_pemilik' },
        { data: 'foto_ktp', name: 'foto_ktp' },
        { data: 'id_warung', name: 'id_warung' },
        { data: 'created_at', name: 'created_at' },
        { data: 'action', name: 'action', orderable: false, searchable: false },
        ],
        language: {
          lengthMenu : '{{ "Menampilkan _MENU_ data" }}',
          zeroRecords : '{{ "Data tidak ditemukan" }}' ,
          info : '{{ "_PAGE_ dari _PAGES_ halaman" }}',
          infoEmpty : '{{ "Data tidak ditemukan" }}',
          infoFiltered : '{{ "(Penyaringan dari _MAX_ data)" }}',
          loadingRecords : '{{ "Memuat data dari server" }}' ,
          processing :    '{{ "Memuat data data" }}',
          search :        '{{ "Pencarian:" }}',
          paginate : {
            first :     '{{ "<" }}' ,
            last :      '{{ ">" }}' ,
            next :      '{{ ">>" }}',
            previous :  '{{ "<<" }}'
          }
        },
        aoColumnDefs: [{
          bSortable: false,
          aTargets: [-1]
        }],
        iDisplayLength: 5,
        aLengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        // sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
        // buttons: ['copy', 'excel', 'csv', 'pdf', 'print'],
      });
    });
  </script>
  @endpush
