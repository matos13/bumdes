<div class="modal-dialog modal-simple">

	{{ Form::model($dataDiri,array('route' => array((!$dataDiri->exists) ? 'data-diri.store':'data-diri.update',$dataDiri->pk()),
	'class'=>'modal-content','id'=>'data-diri-form','method'=>(!$dataDiri->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($dataDiri->exists?'Edit':'Tambah').' Data Diri' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('id_user_apk','hidden')->model($dataDiri)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('user_apk',array('value'=>$dataDiri->exists?(isset($dataDiri->user_apk)?$dataDiri->user_apk->user_apk:null):null))->model(null)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('nik','text')->model($dataDiri)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('jenis_kelamin','text')->model($dataDiri)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('tempat_lahir','text')->model($dataDiri)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('tanggal_lahir','date')->model($dataDiri)->show() !!}
		{!! App\Console\Commands\Generator\Form::textarea( 'alamat_pemilik' )->model($dataDiri)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('nama_pemilik','text')->model($dataDiri)->show() !!}
		{!! App\Console\Commands\Generator\Form::textarea( 'foto_ktp' )->model($dataDiri)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('id_warung','hidden')->model($dataDiri)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('warung',array('value'=>$dataDiri->exists?(isset($dataDiri->warung)?$dataDiri->warung->warung:null):null))->model(null)->show() !!}
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#data-diri-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				
			},
			err: {
				clazz: 'invalid-feedback'
			},
			control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var user_apkEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/user_apk") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#user_apk").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: user_apkEngine.ttAdapter(),
			name: "user_apk",
			displayKey: "user_apk",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{user_apk}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>user_apk tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_user_apk").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			
			var warungEngine = new Bloodhound({
				datumTokenizer: function(d) { return d.tokens; },
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				cache: false,
				remote: {
					url: '{{ url("autocomplete/warung") }}?q=%QUERY',
					wildcard: "%QUERY"
				}
			});

			$("#warung").typeahead({
				hint: true,
				highlight: true,
				minLength: 1
			},
			{
				source: warungEngine.ttAdapter(),
				name: "warung",
				displayKey: "warung",
				templates: {
					suggestion: function(data){
						return Handlebars.compile([
							"<div class=\"tt-dataset\">",
							"<div>@{{warung}}</div>",
							"</div>",
							].join(""))(data);
						},
						empty: [
						"<div>warung tidak ditemukan</div>"
						]
					}
				}).bind("typeahead:selected", function(obj, datum, name) {
					$("#id_warung").val(datum.id);
				}).bind("typeahead:change", function(obj, datum, name) {

				});
				

			});
		</script>
