@extends('layouts.app')

@section('content')
<style>
#chartdiv {
  width: 100%;
  height: 300px;
}
</style>
<div class="container">
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-lg-4">
              <!-- Card -->
              <div class="card p-15 flex-row justify-content-between">
                <div class="white">
                  <i class="icon icon-circle icon-2x md-menu bg-red-600" aria-hidden="true"></i>
                </div>
                <div class="counter counter-md counter text-right">
                  <div class="counter-number-group">
                    <span class="counter-number">{{$barang}}</span>
                    <span class="counter-number-related text-capitalize">Barang</span>
                  </div>
                  <div class="counter-label text-capitalize font-size-16">Total Barang</div>
                </div>
              </div>
              <!-- End Card -->
            </div>

            <div class="col-lg-4">
              <!-- Card -->
              <div class="card p-15 flex-row justify-content-between">
                <div class="counter counter-md text-left">
                  <div class="counter-number-group">
                    <span class="counter-number">{{$warung}}</span>
                    <span class="counter-number-related text-capitalize">Warung</span>
                  </div>
                  <div class="counter-label text-capitalize font-size-16">Total Warung</div>
                </div>
                <div class="white">
                  <i class="icon icon-circle icon-2x md-store bg-blue-600" aria-hidden="true"></i>
                </div>
              </div>
              <!-- End Card -->
            </div> 
            <div class="col-lg-12">
                <!-- Card -->
                <center><h4>Data Penjualan</h4></center>
                <div class="card p-30 flex-row justify-content-between">
                    <div id="chartdiv"></div>
                </div>
              </div>          
          </div>
        </div>
      </div>
</div>
@endsection
@push('js')
    <script>
        am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart);

// Add data
chart.data = [{
  "country": "Januari",
  "visits": 2025
}, {
  "country": "Februari",
  "visits": 1882
}, {
  "country": "Maret",
  "visits": 1809
}, {
  "country": "April",
  "visits": 1322
}, {
  "country": "Mei",
  "visits": 1122
}, {
  "country": "Juni",
  "visits": 1114
}, {
  "country": "Juli",
  "visits": 984
}, {
  "country": "Agustus",
  "visits": 711
}];

// Create axes

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "country";
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.minGridDistance = 30;

categoryAxis.renderer.labels.template.adapter.add("dy", function(dy, target) {
  if (target.dataItem && target.dataItem.index & 2 == 2) {
    return dy + 25;
  }
  return dy;
});

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

// Create series
var series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.valueY = "visits";
series.dataFields.categoryX = "country";
series.name = "Visits";
series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 2;
columnTemplate.strokeOpacity = 1;

}); 
    </script>
@endpush