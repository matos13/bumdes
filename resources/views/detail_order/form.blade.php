<div class="modal-dialog modal-simple">

		{{ Form::model($detailOrder,array('route' => array((!$detailOrder->exists) ? 'detail-order.store':'detail-order.update',$detailOrder->pk()),
	        'class'=>'modal-content','id'=>'detail-order-form','method'=>(!$detailOrder->exists) ? 'POST' : 'PUT')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($detailOrder->exists?'Edit':'Tambah').' Detail Order' }}</h4>
    </div>
    <div class="modal-body">
																				        {!! App\Console\Commands\Generator\Form::input('id_order','hidden')->model($detailOrder)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('order',array('value'=>$detailOrder->exists?(isset($detailOrder->order)?$detailOrder->order->order:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('id_barang','hidden')->model($detailOrder)->showHidden() !!}
{!! App\Console\Commands\Generator\Form::autocomplete('barang',array('value'=>$detailOrder->exists?(isset($detailOrder->barang)?$detailOrder->barang->barang:null):null))->model(null)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('qty','text')->model($detailOrder)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('harga','text')->model($detailOrder)->show() !!}
																        {!! App\Console\Commands\Generator\Form::input('subtotal','text')->model($detailOrder)->show() !!}
																												<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">
$(document).ready(function(){
	$('#detail-order-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	
					var orderEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/order") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#order").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: orderEngine.ttAdapter(),
									name: "order",
									displayKey: "order",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{order}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>order tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_order").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					
					var barangEngine = new Bloodhound({
							datumTokenizer: function(d) { return d.tokens; },
							queryTokenizer: Bloodhound.tokenizers.whitespace,
							cache: false,
							remote: {
								url: '{{ url("autocomplete/barang") }}?q=%QUERY',
								wildcard: "%QUERY"
							}
						});

						$("#barang").typeahead({
									hint: true,
									highlight: true,
									minLength: 1
							},
							{
									source: barangEngine.ttAdapter(),
									name: "barang",
									displayKey: "barang",
									templates: {
										suggestion: function(data){
											return Handlebars.compile([
																"<div class=\"tt-dataset\">",
																		"<div>@{{barang}}</div>",
																"</div>",
														].join(""))(data);
										},
											empty: [
													"<div>barang tidak ditemukan</div>"
											]
									}
							}).bind("typeahead:selected", function(obj, datum, name) {
								$("#id_barang").val(datum.id);
							}).bind("typeahead:change", function(obj, datum, name) {

							});
					

});
</script>
