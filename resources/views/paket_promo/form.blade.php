<div class="modal-dialog modal-simple">

		{{ Form::model($paketPromo,array('route' => array((!$paketPromo->exists) ? 'paket-promo.store':'paket-promo.update',$paketPromo->pk()),
	        'class'=>'modal-content','id'=>'paket-promo-form','method'=>(!$paketPromo->exists) ? 'POST' : 'PUT','enctype'=>'multipart/form-data')) }}

		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
      <h4 class="modal-title" id="formModalLabel">{{ ($paketPromo->exists?'Edit':'Tambah').' Paket Promo' }}</h4>
    </div>
    <div class="modal-body">
	 {!! App\Console\Commands\Generator\Form::input('paket','text')->model($paketPromo)->show() !!}
																      
	<!-- {!! App\Console\Commands\Generator\Form::input('tanggal_awal','text')->model($paketPromo)->show() !!}
	{!! App\Console\Commands\Generator\Form::input('tanggal_akhir','text')->model($paketPromo)->show() !!} -->
	<div class="form-group row">
		<label for="provinsi" class="col-form-label col-md-3">Tanggal Awal</label>
		<div class="col-md-7">
			<div class="input-group date" data-provide="datepicker">
				<input type="text" class="form-control" name="tanggal_awal" value="{{isset($paketPromo->tanggal_awal)?tanggalinput($paketPromo->tanggal_awal):null}}">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="form-group row">
		<label for="provinsi" class="col-form-label col-md-3">Tanggal Akhir</label>
		<div class="col-md-7">
			<div class="input-group date" data-provide="datepicker">
				<input type="text" class="form-control" name="tanggal_akhir" value="{{isset($paketPromo->tanggal_akhir)?tanggalinput($paketPromo->tanggal_akhir):null}}">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<!-- {!! App\Console\Commands\Generator\Form::input('total_harga','text')->model($paketPromo)->show() !!} -->
<div class="form-group row">
	<label for="harga_normal" class="col-form-label col-md-3">Total Harga</label>
	<div class="col-md-7">
		<input name="total_harga" id="total_harga" class="form-control" type="text" value="{{isset($paketPromo->total_harga)?rupiah($paketPromo->total_harga):null}}" data-fv-field="total_harga">
		<small class="invalid-feedback" data-fv-validator="notEmpty" data-fv-for="total_harga" data-fv-result="NOT_VALIDATED" style="display: none;">Kolom harga tidak boleh kosong</small>
	</div>
</div>
<div class="form-group row">
	<label for="kategori_barang" class="col-form-label col-md-3">Gambar</label>
		<div class="col-md-7">
			<input type="file" name="gambar" class="form-control" placeholder="" accept="image/jpeg">
		</div>
	</div>
	<div class="form-group row">
		<label for="provinsi" class="col-form-label col-md-3">Status</label>
		<div class="col-md-7">
			<label class="form-check-label">
				<input type="radio" class="form-check-input" name="status_aktif" value='Y' checked>Aktif
			</label>
			<label class="form-check-label">
				<input type="radio" class="form-check-input" name="status_aktif" value='N' @if ($paketPromo->status_aktif=='N')
				checked
				@endif>Tidak Aktif
			</label>
		</div>
	</div>

		<div class="col-md-12 float-right">
					<div class="text-right">
						<button class="btn btn-primary" id="simpan">Simpan</button>
					</div>
				</div>
		</div>

	    {{ Form::close() }}
</div>


<script type="text/javascript">

$(document).ready(function(){
$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
    startDate: '-3d'
});
	$('#paket-promo-form').formValidation({
	  framework: "bootstrap4",
	  button: {
	    selector: "#simpan",
	    disabled: "disabled"
	  },
	  icon: null,
	  fields: {
	paket : { validators: {
				        notEmpty: {
				          message: 'Kolom paket tidak boleh kosong'
							}
						}
					},status_aktif : { validators: {
				        notEmpty: {
				          message: 'Kolom status_aktif tidak boleh kosong'
							}
						}
					},total_harga : { validators: {
				        notEmpty: {
				          message: 'Kolom total_harga tidak boleh kosong'
							}
						}
					}
},
err: {
	clazz: 'invalid-feedback'
},
control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
	

});
</script>
