<div class="modal-dialog modal-simple">

	{{ Form::model($kabupaten,array('route' => array((!$kabupaten->exists) ? 'kabupaten.store':'kabupaten.update',$kabupaten->pk()),
	'class'=>'modal-content','id'=>'kabupaten-form','method'=>(!$kabupaten->exists) ? 'POST' : 'PUT')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($kabupaten->exists?'Edit':'Tambah').' Kabupaten' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('kode','text')->model($kabupaten)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('kabupaten','text')->model($kabupaten)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('id_provinsi','hidden')->model($kabupaten)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('provinsi',array('value'=>$kabupaten->exists?(isset($kabupaten->provinsi)?$kabupaten->provinsi->provinsi:null):null))->model(null)->show() !!}
		{{--  {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($kabupaten)->show() !!}  --}}
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($kabupaten->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#kabupaten-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				flag_aktif : { validators: {
					notEmpty: {
						message: 'Kolom flag_aktif tidak boleh kosong'
					}
				}
			}
		},
		err: {
			clazz: 'invalid-feedback'
		},
		control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var provinsiEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/provinsi") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#provinsi").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: provinsiEngine.ttAdapter(),
			name: "provinsi",
			displayKey: "provinsi",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{provinsi}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>provinsi tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_provinsi").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			

		});
	</script>
