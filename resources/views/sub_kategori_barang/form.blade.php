<div class="modal-dialog modal-simple">

	{{ Form::model($subKategoriBarang,array('route' => array((!$subKategoriBarang->exists) ? 'sub-kategori-barang.store':'sub-kategori-barang.update',$subKategoriBarang->pk()),
	'class'=>'modal-content','id'=>'sub-kategori-barang-form','method'=>(!$subKategoriBarang->exists) ? 'POST' : 'PUT','enctype'=>'multipart/form-data')) }}

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		<h4 class="modal-title" id="formModalLabel">{{ ($subKategoriBarang->exists?'Edit':'Tambah').' Sub Kategori Barang' }}</h4>
	</div>
	<div class="modal-body">
		{!! App\Console\Commands\Generator\Form::input('id_kategori_barang','hidden')->model($subKategoriBarang)->showHidden() !!}
		{!! App\Console\Commands\Generator\Form::autocomplete('kategori_barang',array('value'=>$subKategoriBarang->exists?(isset($subKategoriBarang->kategori_barang)?$subKategoriBarang->kategori_barang->kategori_barang:null):null))->model(null)->show() !!}
		{!! App\Console\Commands\Generator\Form::input('sub_kategori_barang','text')->model($subKategoriBarang)->show() !!}
		{{-- {!! App\Console\Commands\Generator\Form::textarea( 'icon' )->model($subKategoriBarang)->show() !!} --}}
		{{-- {!! App\Console\Commands\Generator\Form::input('flag_aktif','text')->model($subKategoriBarang)->show() !!} --}}
		<div class="form-group row">
			<label for="kategori_barang" class="col-form-label col-md-3">Icon</label>
			<div class="col-md-7">
				<input type="file" name="icon" class="form-control" placeholder="" accept="image/png">
			</div>
		</div>
		<div class="form-group row">
			<label for="provinsi" class="col-form-label col-md-3">Status</label>
			<div class="col-md-7">
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='Y' checked>Aktif
				</label>
				<label class="form-check-label">
					<input type="radio" class="form-check-input" name="flag_aktif" value='N' @if ($subKategoriBarang->flag_aktif=='N')
					checked
					@endif>Tidak Aktif
				</label>
			</div>
		</div>
		<div class="col-md-12 float-right">
			<div class="text-right">
				<button class="btn btn-primary" id="simpan">Simpan</button>
			</div>
		</div>
	</div>

	{{ Form::close() }}
</div>


<script type="text/javascript">
	$(document).ready(function(){
		$('#sub-kategori-barang-form').formValidation({
			framework: "bootstrap4",
			button: {
				selector: "#simpan",
				disabled: "disabled"
			},
			icon: null,
			fields: {
				flag_aktif : { validators: {
					notEmpty: {
						message: 'Kolom flag_aktif tidak boleh kosong'
					}
				}
			}
		},
		err: {
			clazz: 'invalid-feedback'
		},
		control: {
	// The CSS class for valid control
	valid: 'is-valid',

	// The CSS class for invalid control
	invalid: 'is-invalid'
},
row: {
	invalid: 'has-danger'
}
});
		
		var kategori_barangEngine = new Bloodhound({
			datumTokenizer: function(d) { return d.tokens; },
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			cache: false,
			remote: {
				url: '{{ url("autocomplete/kategori_barang") }}?q=%QUERY',
				wildcard: "%QUERY"
			}
		});

		$("#kategori_barang").typeahead({
			hint: true,
			highlight: true,
			minLength: 1
		},
		{
			source: kategori_barangEngine.ttAdapter(),
			name: "kategori_barang",
			displayKey: "kategori_barang",
			templates: {
				suggestion: function(data){
					return Handlebars.compile([
						"<div class=\"tt-dataset\">",
						"<div>@{{kategori_barang}}</div>",
						"</div>",
						].join(""))(data);
					},
					empty: [
					"<div>kategori_barang tidak ditemukan</div>"
					]
				}
			}).bind("typeahead:selected", function(obj, datum, name) {
				$("#id_kategori_barang").val(datum.id);
			}).bind("typeahead:change", function(obj, datum, name) {

			});
			

		});
	</script>
