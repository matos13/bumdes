<?php

return [
    'list_role' => ['superadministrator','grosir'],
    'role_structure' => [
        'superadministrator' => [
                'card-admin'=>'r',
                'acl-menu' => 'r',
                'users' => 'r',
                'permissions' => 'r',
                'menus' => 'r',
                'master-menu' => 'r',
                'roles' => 'r',
                'profile' => 'r',
                'config-ids'=>'r',
                'config-id'=>'r',
                'perusahaan-menu'=>'r',
                'user-settings-menu'=>'r',
                'provinsi'=>'r',
                'kabupaten'=>'r',
                'kecamatan'=>'r',
                'kelurahan'=>'r',
                'barang-menu' => 'r',
                'kategori-barang' => 'r',
                'sub-kategori-barang' => 'r',
                'grosir' => 'r',
                'barang' => 'r',
                'skema-grosir' => 'r',
                'warung-menu' => 'r',
                'warung' => 'r',
                'user-apk' => 'r',
                'informasi-menu' => 'r',
                'verifikasi-menu' => 'r',
                'order-data' => 'r',
                'minimal-order' => 'r',
                'bumdes' => 'r',
                'presentase-komisi' => 'r',
                'paket-promo' => 'r',
                'detail-paket-promo' => 'r',
                'pembayaran' => 'r',
                'informasi-pembayaran' => 'r',
        ],
        'grosir'=>[
            'card-admin'=>'r',
                'menus' => 'r',
                'profile' => 'r',
                'barang-menu' => 'r',
                'kategori-barang' => 'r',
                'sub-kategori-barang' => 'r',
                'grosir' => 'r',
                'barang' => 'r',
                'skema-grosir' => 'r',
                'warung-menu' => 'r',
                'warung' => 'r',
                'verifikasi-menu' => 'r',
                'order-data' => 'r',
        ],
    ],
    'permissions_map' => [
        'r' => 'read'
    ]

];
